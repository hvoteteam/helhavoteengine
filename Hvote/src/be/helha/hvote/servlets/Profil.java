package be.helha.hvote.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import be.helha.hvote.domaine.Candidat;
import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.exceptions.ArgumentInvalideException;
import be.helha.hvote.usecases.GestionElecteur;
import be.helha.hvote.usecases.GestionProgramme;

/**
 * Servlet implementation class Profil
 */

@WebServlet(urlPatterns = "/profil.html", name = "profil.html")
public class Profil extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String VIEW = "/WEB-INF/jsp/profil.jsp";

	@EJB
	private GestionElecteur gestionElecteur;
	@EJB
	private GestionProgramme gestionProg;

	public Profil() {
		super();
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		
		synchronized (session) {
			Electeur electeur= (Electeur) session.getAttribute(Constantes.ATT_USER);
			request.setAttribute(Constantes.ATT_USER, electeur);
			request.setAttribute(Constantes.ATT_TITRE, "Profil");
			this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		
		String nom = request.getParameter(Constantes.ATT_NOM);
		String prenom = request.getParameter(Constantes.ATT_PRENOM);
		String banniere ="https://www.tccd.edu/magazine/assets/images/volume-05/issue-02/midterm-elections/midterm-elections-matter-header-large.jpg";
		String photo = "http://ssl.gstatic.com/accounts/ui/avatar_2x.png";
		
		String message = "";
		Electeur electeur= (Electeur) session.getAttribute(Constantes.ATT_USER);
		Candidat candidat = null;
		
		
		try {
			candidat = new Candidat(nom,prenom,photo,banniere);
		} catch (ArgumentInvalideException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		
		if(nom.isEmpty() || prenom.isEmpty() ) {
			message ="Veuillez introduire vos informations.";
		}
		else {
			candidat.setNom(nom);
			candidat.setPrenom(prenom);
			electeur.setCandidat(candidat);
			this.gestionElecteur.update(electeur);	
			
		}
		request.setAttribute(Constantes.ATT_USER, electeur);
		request.setAttribute(Constantes.ATT_MESSAGE, message);
		this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
		

		
		
	}

}