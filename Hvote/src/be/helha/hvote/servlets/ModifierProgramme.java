package be.helha.hvote.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.domaine.Election;
import be.helha.hvote.domaine.Programme;
import be.helha.hvote.usecases.GestionElecteur;
import be.helha.hvote.usecases.GestionElection;
import be.helha.hvote.usecases.GestionProgramme;


@WebServlet(urlPatterns = "/modifProgramme.html", name = "modifProgramme.html")
public class ModifierProgramme extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String VIEW = "/WEB-INF/jsp/modifierProgramme.jsp";
	@EJB
	private GestionElection gestionElection;
	@EJB
	private GestionProgramme gestionProg;
	@EJB
	private GestionElecteur gestionElecteur;
   
    public ModifierProgramme() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		String message = "";
		
		synchronized (session) {
			
			Electeur electeur = (Electeur) session.getAttribute(Constantes.ATT_USER);
			if(electeur != null && electeur.isConnecte()) {
				request.setAttribute(Constantes.ATT_USER, electeur);
				request.setAttribute(Constantes.ATT_MESSAGE, message);
				this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
			}
			else {
				electeur = null;
				session.removeAttribute(Constantes.ATT_USER);
				
				response.sendRedirect( request.getContextPath() + "/accueil.html" );
			}
			
			
		}
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		String sujet = request.getParameter(Constantes.ATT_ELECTION);
		String electeur = request.getParameter(Constantes.ATT_USER);
		String message = "";
		Election election = null;
		
		String text = request.getParameter(Constantes.ATT_TEXT_PROG);
		
		String[] parts1 = electeur.split("=", -1);
		String partWithUser = parts1[2];
		String[] parts2 = partWithUser.split(",", -1);
		String user = parts2[0];		//mail de l user recupere
		
		election = this.gestionElection.rechercherElection(sujet);
		election = this.gestionElection.chargerTout(election);
		List<Programme> listP = election.getProgrammes();
		for(Programme p : listP) {
			if(p.getCandidat().getPseudoMail().equals(user)) {
				
				if(text.isEmpty()) {
					p = this.gestionProg.updateProgTexte(p, "Votre programme");
					
				}
				else {
					if(text != null) {
						p = this.gestionProg.updateProgTexte(p, text);
					}
					
					
				}
				
			}
		}
		
		VIEW = "/WEB-INF/jsp/index.jsp";
		request.setAttribute(Constantes.ATT_USER, electeur);
		request.setAttribute(Constantes.ATT_MESSAGE, message);
		
		response.sendRedirect( request.getContextPath() + "/accueil.html" );
		
	}

}
