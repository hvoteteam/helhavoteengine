package be.helha.hvote.servlets;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import be.helha.hvote.dao.ElecteurDao;
import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.exceptions.ArgumentInvalideException;
import be.helha.hvote.usecases.GestionElecteur;



@SuppressWarnings("serial")
@WebServlet(urlPatterns="/inscription.html", name="inscription.html")
public class Inscription extends HttpServlet {
	private static String VIEW = "/WEB-INF/jsp/inscription.jsp";
	@EJB
	private GestionElecteur gestionElec;
	
	public Inscription() {
        super();
    }
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
		
		HttpSession session = request.getSession(true);
		String message = "";
		
		synchronized (session) {
			Electeur electeur = (Electeur) session.getAttribute(Constantes.ATT_USER);
			if(electeur != null && electeur.isConnecte()) {
				VIEW = "/accueil.html";
			} else {
				electeur = null;
				session.removeAttribute(Constantes.ATT_USER);
			}
			
			request.setAttribute(Constantes.ATT_USER, electeur);
			request.setAttribute(Constantes.ATT_MESSAGE, message);
			this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
			}
		}
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
		
		HttpSession session = request.getSession(true);
		String login = request.getParameter(Constantes.ATT_LOGIN);
		String password = request.getParameter(Constantes.ATT_PSWD);
		String passwordConfirmation = request.getParameter(Constantes.ATT_PSWD_2);
		String message = "";
		Electeur electeur=null;
		String regex = "^(.+)@(.+)$";
		Pattern pattern = Pattern.compile(regex);
		try {
			electeur = new Electeur(false, login, password);
		} catch (ArgumentInvalideException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		if(login.isEmpty() && password.isEmpty() && passwordConfirmation.isEmpty()) {
			message ="Veuillez compl�ter le formulaire.";
		}
		else {
			Matcher matcher = pattern.matcher(login);
		if(matcher.matches()) {
			
			if(password.equals(passwordConfirmation)) {
				if(password.length() > 3) {
					electeur = this.gestionElec.enregistrer(electeur);

					synchronized (session) {			
						if(electeur != null && !electeur.getPasswd().equals(password)) {
							session.setAttribute(Constantes.ATT_USER, electeur);
							message = "Vous pouvez d�sormais vous connecter avec cette adresse mail: " + login;
						} else {
							message = "Malheureusement cette adresse mail est d�j� utilis�.";
						}
					}
				} else {
					message = "Il faut au moins 4 caract�res pour le mot de passe.";
				}
			} else {
				message = "Le mot de passe et sa confirmation ne sont pas �quivalents.";
			}
		} else {
			message = "L'adresse mail n'est pas valide.";
		}
		}
		
		request.setAttribute(Constantes.ATT_USER, electeur);
		request.setAttribute(Constantes.ATT_MESSAGE, message);
		this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
		
		
			
		
	}

}
