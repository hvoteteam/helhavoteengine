package be.helha.hvote.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.domaine.Election;
import be.helha.hvote.domaine.Programme;
import be.helha.hvote.exceptions.ArgumentInvalideException;
import be.helha.hvote.usecases.GestionElecteur;
import be.helha.hvote.usecases.GestionElection;
import be.helha.hvote.usecases.GestionProgramme;

/**
 * Servlet implementation class CreationElec
 */

@WebServlet(urlPatterns="/creationElec.html", name="creationElec.html")
public class CreationElec extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String VIEW = "/WEB-INF/jsp/creationElec.jsp";
	@EJB
	private GestionElection gestionElection;
	@EJB
	private GestionElecteur gestionElecteur;
	@EJB
	private GestionProgramme gestionProg;
	
	@Resource(lookup = "java:/jboss/mail/gmail")
	private Session mailSession;
       
	public CreationElec() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		///Check si user est conect� et peut donc acc�der � la page
		HttpSession session = request.getSession(true);
		String message = "";
		
		synchronized (session) {
			Electeur electeur = (Electeur) session.getAttribute(Constantes.ATT_USER);
			if(electeur != null && electeur.isConnecte()) {
				
				List<Electeur> listeElec = gestionElecteur.listerElecteurs();
				request.setAttribute(Constantes.ATT_LISTE_ALL_ELECTEURS,listeElec);
				List<Electeur> listeCand = new ArrayList<Electeur>(listeElec);
				listeCand.remove(electeur);
				request.setAttribute(Constantes.ATT_LISTE_ALL_CANDIDATS,listeCand);
				request.setAttribute(Constantes.ATT_USER, electeur);
				request.setAttribute(Constantes.ATT_MESSAGE, message);
			
				VIEW = "/WEB-INF/jsp/creationElec.jsp";
				this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
			} 
			else {
				
				electeur = null;
				session.removeAttribute(Constantes.ATT_USER);
				
				response.sendRedirect( request.getContextPath() + "/accueil.html" );
			}
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		String sujet = request.getParameter(Constantes.ATT_SUJET);
		String addElect = request.getParameter(Constantes.ATT_ADDELECT);
		Set<String> hash_Set = new HashSet<String>();					//--> sans doublons
		Set<String> tree_Set = null;									//--> tri�
		String message = "";
		Electeur admin = null;
		Electeur candid = null;
		Election election = null;
		String addCandidat = request.getParameter(Constantes.ATT_ADDCANDIDAT);
		Set<String> hash_Set_Candidat = new HashSet<String>();
		Set<String> tree_Set_Candidat = null;
		Election electprog=null;
		
		
		String electionUrlBan = "";
		String electionCouleur = "";
		electionCouleur = request.getParameter(Constantes.ATT_ELECTION_COULEUR);
		electionUrlBan = request.getParameter(Constantes.ATT_ELECTION_URL_BAN);
	
		
		String nbToursP = request.getParameter(Constantes.ATT_RB_NBTOURS);
		String deb1 = request.getParameter(Constantes.ATT_DATE_DEB1); 
		String deb2 = request.getParameter(Constantes.ATT_DATE_DEB2);
		String fin1 = request.getParameter(Constantes.ATT_DATE_FIN1);
		String fin2 = request.getParameter(Constantes.ATT_DATE_FIN2);
		
		Date ddeb1 = null;
		Date dfin1 = null;
		Date ddeb2 = null;
		Date dfin2 = null;
		Date now = new Date();
		int errorDate = 0;
		
		
		/**Rech admin**/
		Electeur user = (Electeur) session.getAttribute(Constantes.ATT_USER);
		admin = this.gestionElecteur.rechercherElecteur(user.getPseudoMail());
		
		
		
		/**Verif sujet**/
		if(sujet.isEmpty()) {
			message = "Veuillez compl�ter le formulaire.";
			request.setAttribute(Constantes.ATT_USER, user);
			request.setAttribute(Constantes.ATT_MESSAGE, message);
			List<Electeur> listeElec = gestionElecteur.listerElecteurs();
			request.setAttribute(Constantes.ATT_LISTE_ALL_ELECTEURS,listeElec);
			List<Electeur> listeCand = new ArrayList<Electeur>(listeElec);
			listeCand.remove(user);
			request.setAttribute(Constantes.ATT_LISTE_ALL_CANDIDATS,listeCand);
			this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
		}
		else {
			/**Elec existe deja**/
			Election checkelec = this.gestionElection.rechercherElection(sujet);
			if(checkelec != null) {
				message = "Il y a d�j� une �lection sur ce sujet.";
				request.setAttribute(Constantes.ATT_USER, user);
				request.setAttribute(Constantes.ATT_MESSAGE, message);
				List<Electeur> listeElec = gestionElecteur.listerElecteurs();
				request.setAttribute(Constantes.ATT_LISTE_ALL_ELECTEURS,listeElec);
				List<Electeur> listeCand = new ArrayList<Electeur>(listeElec);
				listeCand.remove(user);
				request.setAttribute(Constantes.ATT_LISTE_ALL_CANDIDATS,listeCand);
				this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
			}
			else {	/**verif des dates***/
				if(deb1.isEmpty() || fin1.isEmpty()) {				//dates t1 pas completees
					message = "Veuillez compl�ter le formulaire. Une des dates �tait vide.";
					errorDate=1;
					request.setAttribute(Constantes.ATT_USER, user);
					request.setAttribute(Constantes.ATT_MESSAGE, message);
					List<Electeur> listeElec = gestionElecteur.listerElecteurs();
					request.setAttribute(Constantes.ATT_LISTE_ALL_ELECTEURS,listeElec);
					List<Electeur> listeCand = new ArrayList<Electeur>(listeElec);
					listeCand.remove(user);
					request.setAttribute(Constantes.ATT_LISTE_ALL_CANDIDATS,listeCand);
					this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
				}
				else {
					try {
						ddeb1 = new SimpleDateFormat("yyyy-MM-dd").parse(deb1);
						dfin1 = new SimpleDateFormat("yyyy-MM-dd").parse(fin1);
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} 
					if(!dfin1.after(ddeb1)) {
						message = "Veuillez compl�ter le formulaire. La date de fin n'�tait pas apr�s la date de d�but. ";
						errorDate=1;
						request.setAttribute(Constantes.ATT_USER, user);
						request.setAttribute(Constantes.ATT_MESSAGE, message);
						List<Electeur> listeElec = gestionElecteur.listerElecteurs();
						request.setAttribute(Constantes.ATT_LISTE_ALL_ELECTEURS,listeElec);
						List<Electeur> listeCand = new ArrayList<Electeur>(listeElec);
						listeCand.remove(user);
						request.setAttribute(Constantes.ATT_LISTE_ALL_CANDIDATS,listeCand);
						this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
					}
					else {
						if(!now.before(dfin1)) {
							message = "Veuillez compl�ter le formulaire. La date de fin �tait avant aujourd'hui.";
							errorDate=1;
							request.setAttribute(Constantes.ATT_USER, user);
							request.setAttribute(Constantes.ATT_MESSAGE, message);
							List<Electeur> listeElec = gestionElecteur.listerElecteurs();
							request.setAttribute(Constantes.ATT_LISTE_ALL_ELECTEURS,listeElec);
							List<Electeur> listeCand = new ArrayList<Electeur>(listeElec);
							listeCand.remove(user);
							request.setAttribute(Constantes.ATT_LISTE_ALL_CANDIDATS,listeCand);
							this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
						}
						else {
							if(Integer.parseInt(nbToursP)==2) {
								if(deb2.isEmpty() || fin2.isEmpty()) {			//dates t2 pas completees si y a 2t
									message = "Veuillez compl�ter le formulaire. Une des dates �tait vide.";
									errorDate=1;
									request.setAttribute(Constantes.ATT_USER, user);
									request.setAttribute(Constantes.ATT_MESSAGE, message);
									List<Electeur> listeElec = gestionElecteur.listerElecteurs();
									request.setAttribute(Constantes.ATT_LISTE_ALL_ELECTEURS,listeElec);
									List<Electeur> listeCand = new ArrayList<Electeur>(listeElec);
									listeCand.remove(user);
									request.setAttribute(Constantes.ATT_LISTE_ALL_CANDIDATS,listeCand);
									this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
								}
								else {
									try {
										ddeb1 = new SimpleDateFormat("yyyy-MM-dd").parse(deb1);
										dfin1 = new SimpleDateFormat("yyyy-MM-dd").parse(fin1);
										ddeb2 = new SimpleDateFormat("yyyy-MM-dd").parse(deb2);
										dfin2 = new SimpleDateFormat("yyyy-MM-dd").parse(fin2);
									} catch (ParseException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									} 
									if(!dfin2.after(ddeb2)) {
										message = "Veuillez compl�ter le formulaire. La date de fin n'�tait pas apr�s la date de d�but.";
										errorDate=1;
										request.setAttribute(Constantes.ATT_USER, user);
										request.setAttribute(Constantes.ATT_MESSAGE, message);
										List<Electeur> listeElec = gestionElecteur.listerElecteurs();
										request.setAttribute(Constantes.ATT_LISTE_ALL_ELECTEURS,listeElec);
										List<Electeur> listeCand = new ArrayList<Electeur>(listeElec);
										listeCand.remove(user);
										request.setAttribute(Constantes.ATT_LISTE_ALL_CANDIDATS,listeCand);
										this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
									}
									else {
										if(!ddeb2.after(dfin1)) {
											message = "Veuillez compl�ter le formulaire. Le d�but du tour 2 �tait avant la fin du tour 1.";
											errorDate=1;
											request.setAttribute(Constantes.ATT_USER, user);
											request.setAttribute(Constantes.ATT_MESSAGE, message);
											List<Electeur> listeElec = gestionElecteur.listerElecteurs();
											request.setAttribute(Constantes.ATT_LISTE_ALL_ELECTEURS,listeElec);
											List<Electeur> listeCand = new ArrayList<Electeur>(listeElec);
											listeCand.remove(user);
											request.setAttribute(Constantes.ATT_LISTE_ALL_CANDIDATS,listeCand);
											this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
										
										}
										else {
											if(!now.before(dfin2)) {
												message = "Veuillez compl�ter le formulaire. La date de fin �tait avant aujourd'hui.";
												errorDate=1;
												request.setAttribute(Constantes.ATT_USER, user);
												request.setAttribute(Constantes.ATT_MESSAGE, message);
												List<Electeur> listeElec = gestionElecteur.listerElecteurs();
												request.setAttribute(Constantes.ATT_LISTE_ALL_ELECTEURS,listeElec);
												List<Electeur> listeCand = new ArrayList<Electeur>(listeElec);
												listeCand.remove(user);
												request.setAttribute(Constantes.ATT_LISTE_ALL_CANDIDATS,listeCand);
												this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
											}
										}
									}
								}
							}
						}
					}
					
			
					
				}
				
			}/**fin verif dates**/
				
			if(errorDate==0) {
				/**Creer election**/
				if(Integer.parseInt(nbToursP)==2) {
					String cantvote = request.getParameter(Constantes.ATT_CB_DROIT);
					if(cantvote==null) {
						try {
							election = new Election(sujet, admin, Integer.parseInt(nbToursP), 0);
							
							if(electionCouleur != null) {
								election = this.gestionElection.setCouleur(election, electionCouleur);
								election.setTheme(electionCouleur);
							}					
							if(electionUrlBan != null) {
								election = this.gestionElection.setBanniere(election, electionUrlBan);
							}
						} catch (ArgumentInvalideException e) {
								
						}
					}
					else {
						if(cantvote.equals("nomore")) {
							System.out.println("cant= "+cantvote);
							try {
								election = new Election(sujet, admin, Integer.parseInt(nbToursP), 1);
								
								if(electionCouleur != null) {
									election = this.gestionElection.setCouleur(election, electionCouleur);
									election.setTheme(electionCouleur);
								}					
								if(electionUrlBan != null) {
									election = this.gestionElection.setBanniere(election, electionUrlBan);
								}
							} catch (ArgumentInvalideException e) {
									
							}
						}
					}
				}
				else {
					try {
						election = new Election(sujet, admin, Integer.parseInt(nbToursP), 0);
						
						if(electionCouleur != null) {
							election = this.gestionElection.setCouleur(election, electionCouleur);
							election.setTheme(electionCouleur);
						}					
						if(electionUrlBan != null) {
							election = this.gestionElection.setBanniere(election, electionUrlBan);
						}
					} catch (ArgumentInvalideException e) {
							
					}
				}
				
				
				/*if(cantvote.equals("nomore")) {
					System.out.println("cant= "+cantvote);
					try {
						election = new Election(sujet, admin, Integer.parseInt(nbToursP), 1);
					} catch (ArgumentInvalideException e) {
							
					}
				}
				
				else {
					try {
						election = new Election(sujet, admin, Integer.parseInt(nbToursP), 0);
					} catch (ArgumentInvalideException e) {
							
					}
				}*/
				
				
				
				election.setStartDate1(ddeb1);
				election.setEndDate1(dfin1);
				if(Integer.parseInt(nbToursP)==2) {
					election.setStartDate2(ddeb2);
					election.setEndDate2(dfin2);
				}
				
				/**Verif candidats**/
				if(addCandidat.isEmpty()) {
					message = "Veuillez compl�ter le formulaire.";
					request.setAttribute(Constantes.ATT_USER, user);
					request.setAttribute(Constantes.ATT_MESSAGE, message);
					List<Electeur> listeElec = gestionElecteur.listerElecteurs();
					request.setAttribute(Constantes.ATT_LISTE_ALL_ELECTEURS,listeElec);
					List<Electeur> listeCand = new ArrayList<Electeur>(listeElec);
					listeCand.remove(user);
					request.setAttribute(Constantes.ATT_LISTE_ALL_CANDIDATS,listeCand);
					this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
				
				}
				if(!addCandidat.isEmpty()) {
					String[] partsCand = addCandidat.split(",", -1);	//recup candids
					for(String p : partsCand) {		
						
						hash_Set_Candidat.add(p);
					}
					tree_Set_Candidat = new TreeSet<String>(hash_Set_Candidat);		//On a fait une liste sans doublons des candids
					for(String p2 : tree_Set_Candidat) {
						candid = this.gestionElecteur.rechercherElecteur(p2);	//rech son compte elect
						
						try {
							
							Programme progTemp = this.gestionProg.defineCandid(new Programme("Votre programme"), candid);	//defini comme candid avec prog par def
							
							Programme pro = this.gestionProg.sauver(progTemp);		//Sauve le prog
							
							electprog = this.gestionElection.addProg(election, pro);	//lie prog � election en cours
							
						} catch (ArgumentInvalideException e) {
							e.printStackTrace();
						}
						Electeur cand2 = this.gestionElecteur.chargerTout(candid);
						//System.out.println("candprog" +cand2.getCandidat().getProgrammes());
						
						Election election2 = this.gestionElection.sauver(electprog);
						Election election3 = this.gestionElection.chargerTout(election2);
						//System.out.println(election3.getSujet()+" : "+election3.getProgrammes());
						
					}
					
					
					/*****************************************************************************************************
					 *  NOTES IMPORTANTES:
					 *  Le code suivant fonctionne,
					 *  il a juste �t� mis en commentaire pour le bien de la pr�sentation
					 *  car envoyer des mails prend du temps.
					 *  
					 *****************************************************************************************************/
					
					   PrintWriter out = response.getWriter();
				       try {
				    	   MimeMessage m = new MimeMessage(mailSession);
				    	   Address from = new InternetAddress("helhavoteengine@gmail.com");
				    	  
				    	   for(String smail : tree_Set_Candidat) {
				    		   Address to = new InternetAddress(smail);
				    		   
				    		   m.setFrom(from);
				    		   m.setRecipient(Message.RecipientType.TO, to);
				    		   m.setSubject("Candidat d'un vote HVE");
				    		   m.setContent("Vous avez �t� d�sign� comme candidat pour l'�lection "+sujet+".Venez donc voter sur http://localhost:8080/Hvote/" , "text/plain");
				    		   Transport.send(m);
				    		   System.out.println("Mail candidat envoy� �: "+smail);
				    		   
				    	   }
				       } catch (javax.mail.MessagingException e) {
				    	   e.printStackTrace(out);
				       }
				      
					/**
					 * FIN DE LA NOTE
					 */
				}
				
				
				
				
				VIEW = "/WEB-INF/jsp/index.jsp";
				
				
				/**Recup des electeurs pour les inviter**/
				if(addElect!=null) {
					String[] parts = addElect.split(",", -1);
			        for (String p : parts) {
			        	
			        	hash_Set.add(p);
			        }
			        tree_Set = new TreeSet<String>(hash_Set);
			        
			        
			        
			        /**********************************************************************************************************
					 *  NOTES IMPORTANTES:
					 *  Le code suivant fonctionne,
					 *  il a juste �t� mis en commentaire pour le bien de la pr�sentation
					 *  car envoyer des mails prend du temps.
					 *  
					 **********************************************************************************************************/
			        /*
			       PrintWriter out = response.getWriter();
			       try {
			    	   MimeMessage m = new MimeMessage(mailSession);
			    	   Address from = new InternetAddress("helhavoteengine@gmail.com");
			    	  
			    	   for(String smail : tree_Set) {
			    		   Address to = new InternetAddress(smail);
			    		   
			    		   m.setFrom(from);
			    		   m.setRecipient(Message.RecipientType.TO, to);
			    		   m.setSubject("Invitation au vote HVE");
			    		   m.setContent("Vous avez �t� invit� � donner votre avis pour l'�lection "+sujet+".Venez donc voter sur http://localhost:8080/Hvote/" , "text/plain");
			    		   Transport.send(m);
			    		   System.out.println("Mail envoy� �: "+smail);
			    		   
			    	   }
			       } catch (javax.mail.MessagingException e) {
			    	   e.printStackTrace(out);
			       }
			        */
			        /**
			         * FIN DE LA NOTE
			         */
				}
				
				
				request.setAttribute(Constantes.ATT_USER, user);
				request.setAttribute(Constantes.ATT_MESSAGE, message);
				
				response.sendRedirect( request.getContextPath() + "/accueil.html" );
				
			}

			
			
		
		}
		
		
		
	}

}
