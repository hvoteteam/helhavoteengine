package be.helha.hvote.servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.exceptions.ArgumentInvalideException;
import be.helha.hvote.usecases.GestionElecteur;

@SuppressWarnings("serial")
@WebServlet(urlPatterns="/login.html", name="login.html")
public class Login extends HttpServlet{
	private static String VIEW = "/WEB-INF/jsp/login.jsp";
	@EJB
	private GestionElecteur gestionElec;
	
	public Login() {
	        super();
	    }
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
		HttpSession session = request.getSession(true);
		String message = "";
		
		synchronized (session) {
			Electeur electeur = (Electeur) session.getAttribute(Constantes.ATT_USER);
			if(electeur != null && electeur.isConnecte()) {
				VIEW = "/index.html";
			} else {
				electeur = null;
				session.removeAttribute(Constantes.ATT_USER);
			}
			
			request.setAttribute(Constantes.ATT_USER, electeur);
			request.setAttribute(Constantes.ATT_MESSAGE, message);
			this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
		}
	}
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
		HttpSession session = request.getSession(true);
		String login = request.getParameter(Constantes.ATT_LOGIN);
		String password = request.getParameter(Constantes.ATT_PSWD);
		Electeur electeur=null;
		String message = "";
		if(!login.isEmpty() && !password.isEmpty()) {
			
			try {
				electeur = new Electeur(false, login, password );
			} catch (ArgumentInvalideException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
			//System.out.println(user.toString());
			electeur = this.gestionElec.connecter(electeur);
			
			
			synchronized (session) {
				session.setAttribute(Constantes.ATT_USER, electeur);
				if(electeur != null && electeur.isConnecte()) {
					message = "Vous venez de vous connecter en tant que " + electeur.getPseudoMail() + ".";
				} else {
					message = "L'adresse mail ou le mot de passe est incorrect.";
				}
				
				request.setAttribute(Constantes.ATT_USER, electeur);
				request.setAttribute(Constantes.ATT_MESSAGE, message);
				this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
			}
			
			
		}
		else {
			if(login.isEmpty() && password.isEmpty()) {
				message = "Veuillez compl�ter le formulaire.";
			}
			else {
				if(password.isEmpty()) {
					message = "Veuillez ins�rer votre mot de passe.";
				}
				else {
					message = "Veuillez ins�rer votre adresse mail.";
				}
				
			}
			request.setAttribute(Constantes.ATT_MESSAGE, message);
			this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
			
			
		}
		
	}

}
