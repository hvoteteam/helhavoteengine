package be.helha.hvote.servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.domaine.Election;
import be.helha.hvote.usecases.GestionElection;


@WebServlet(urlPatterns = "/modifElection.html", name = "modifElection.html")
public class ModifierElection extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String VIEW = "/WEB-INF/jsp/modifierElection.jsp";
	@EJB
	private GestionElection gestionElection;
   
    public ModifierElection() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		String message = "";
		
		synchronized (session) {
			
			Electeur electeur = (Electeur) session.getAttribute(Constantes.ATT_USER);
			if(electeur != null && electeur.isConnecte()) {
				request.setAttribute(Constantes.ATT_USER, electeur);
				request.setAttribute(Constantes.ATT_MESSAGE, message);
				this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
			}
			else {
				electeur = null;
				session.removeAttribute(Constantes.ATT_USER);
				
				response.sendRedirect( request.getContextPath() + "/accueil.html" );
			}
			
			
		}
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		String sujet = request.getParameter(Constantes.ATT_ELECTION);
		String electeur = request.getParameter(Constantes.ATT_USER);
		String message = "";
		Election election = null;
		
		String electionUrlBan = "";
		String electionCouleur = "";
		electionCouleur = request.getParameter(Constantes.ATT_ELECTION_COULEUR);
		electionUrlBan = request.getParameter(Constantes.ATT_ELECTION_URL_BAN);
		
		election = this.gestionElection.rechercherElection(sujet);
		
		if(electionCouleur != null) {
			election = this.gestionElection.updateCouleur(election, electionCouleur);
			election.setTheme(electionCouleur);
		}					
		if(electionUrlBan != null) {
			election = this.gestionElection.updateBanniere(election, electionUrlBan);
			
		}
		
		
		VIEW = "/WEB-INF/jsp/index.jsp";
		request.setAttribute(Constantes.ATT_USER, electeur);
		request.setAttribute(Constantes.ATT_MESSAGE, message);
		
		response.sendRedirect( request.getContextPath() + "/accueil.html" );
		
	}

}
