package be.helha.hvote.servlets;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import be.helha.hvote.domaine.Block;
import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.domaine.Election;
import be.helha.hvote.domaine.Programme;
import be.helha.hvote.domaine.VoteEnt;
import be.helha.hvote.usecases.GestionBlockchain;
import be.helha.hvote.usecases.GestionElecteur;
import be.helha.hvote.usecases.GestionElection;
import be.helha.hvote.usecases.GestionProgramme;
import be.helha.hvote.usecases.GestionVote;

/**
 * Servlet implementation class Vote
 */

@WebServlet(urlPatterns="/vote.html", name="vote.html")
public class Vote extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String VIEW = "/WEB-INF/jsp/vote.jsp";
	@EJB
	private GestionElection gestionElection;
	@EJB
	private GestionElecteur gestionElecteur;
	@EJB
	private GestionProgramme gestionProg;
	@EJB
	private GestionBlockchain gestionBlockchain;
	@EJB
	private GestionVote gestionVote;
	
	public Vote() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		///Check si user est connecte et peut donc acceder a la page
		/*HttpSession session = request.getSession(true);
		String message = "";

		synchronized (session) {
			Electeur electeur = (Electeur) session.getAttribute(Constantes.ATT_USER);
			if(electeur != null && electeur.isConnecte()) {

				String sujet = (String) request.getAttribute(Constantes.ATT_ELECTION);
				System.out.println("sujInDoGet: "+ sujet);
				System.out.println("elec: "+ electeur);
				
				//get les candidats et leur programme pour l'afficher
				//probablement une liste de d'objets candidats 
				//et une liste de programmes dans le mm ordre que la liste au dessus
				
				List<Programme> listProg = (List<Programme>) request.getAttribute(Constantes.ATT_LISTE_ALL_PROGS);
				request.setAttribute(Constantes.ATT_LISTE_ALL_PROGS, listProg);
				
				List<Electeur> listCand = (List<Electeur>) request.getAttribute(Constantes.ATT_LISTE_ALL_CANDIDATS);
				request.setAttribute(Constantes.ATT_LISTE_ALL_CANDIDATS, listCand);
				
				request.setAttribute(Constantes.ATT_ELECTION, sujet);

				
			} 
			else {

				electeur = null;
				session.removeAttribute(Constantes.ATT_USER);

				response.sendRedirect( request.getContextPath() + "/accueil.html" );
			}
			
			
			request.setAttribute(Constantes.ATT_USER, electeur);
			request.setAttribute(Constantes.ATT_MESSAGE, message);
			this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
		}*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(true);
		String sujet = request.getParameter(Constantes.ATT_ELECTION);
		
		String electeur = request.getParameter(Constantes.ATT_USER);
		String progChoisi = request.getParameter(Constantes.ATT_PROG_CHOISI);
		String message = "";
		String voteLine = "";
		
		Electeur candid = null;
		Election election = null;
		Block bloc = null;
		Election elecProgMod = null;
		
		election = this.gestionElection.rechercherElection(sujet);
		
		
		
		Programme prog = this.gestionProg.rechercherProg(Integer.parseInt(progChoisi));		//recherche programme choisi
		candid = prog.getCandidat();														//recherche candid correspondant
		
		voteLine = "L'�lecteur: "+electeur+" a vot� pour: "+candid.getPseudoMail()+" dans l'�lection: "+sujet+" au tour: "+election.getTourActuel();
		
		//System.out.println("vote: "+voteLine);
		
		if(election.getTourActuel()==1) {
			
			//vote au t1
			elecProgMod = this.gestionElection.votedForProg(election, prog);			//Incremente cpt de vote pour le programme
	
		}
		else {
			if(election.getTourActuel()==2) {
				
				//vote au t2
				elecProgMod = this.gestionElection.votedForProgT2(election, prog);			//Incremente cpt T2 de vote pour le programme
			
			}
		}
		
		
		

		
		
		
		Election elecCharg = this.gestionElection.chargerChain(elecProgMod);
		
		List<Block> chain = elecCharg.getBlockchain();
		
		if(chain.isEmpty()) {
			try {
				bloc = new Block(voteLine,"",genererCle());									//1er elem de la chaine, pas de hashprec
				
			} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			Block blockPrec = chain.get(chain.size()-1);									//recup hash prec
			
			try {
				
				bloc = new Block(voteLine,blockPrec.getHash(),genererCle());
				
			} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		Block blocSaved = this.gestionBlockchain.sauver(bloc);
		Election elecBlock = this.gestionElection.addBlock(election, blocSaved);					//sauve block, l'ajoute a l'elec 
		
		int nbTours = elecBlock.getTourActuel();
		
		VoteEnt voteEnt = new VoteEnt(nbTours);
		Electeur voter = this.gestionElecteur.rechercherElecteur(electeur);
		VoteEnt voteWithVoter = this.gestionVote.defineElecteur(voteEnt, voter);
		VoteEnt voteSaved = this.gestionVote.sauver(voteWithVoter);									//cree vote, ajoute electeur, sauve
		
		Election elecWithVote = this.gestionElection.addVote(elecBlock, voteSaved);					//ajoute vote a election
		
		Election elecSaved = this.gestionElection.sauver(elecWithVote);								//persiste tout			
		
	
		
		
		
		//Une fois le vote fait, on prouve le vote a ete fait sur la page verif
		String raw = "";
		message = "Votre vote a bien �t� enregistr�.";
		Election elecCharge = this.gestionElection.chargerChain(elecSaved);
		List<Block> listBlock = elecCharge.getBlockchain();
		List<String> listToDisplay = new ArrayList<String>();
		
		for(Block b : listBlock) {
			try {
				raw = b.decrypter(b.getData(), b.getCle());
			} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
					| IllegalBlockSizeException | BadPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String[] parts = raw.split(" ", -1);
			
			if(parts[1].equals(electeur)) {
				listToDisplay.add(raw);
			}
			else {
				listToDisplay.add(b.getData().toString());
			}
		}
		
		request.setAttribute(Constantes.ATT_BLOCKCHAIN, listToDisplay);
		
		request.setAttribute(Constantes.ATT_ELECTION, election.getSujet());
		request.setAttribute(Constantes.ATT_USER, electeur);
		request.setAttribute(Constantes.ATT_MESSAGE, message);
		VIEW = "/WEB-INF/jsp/verificationVote.jsp";
	
		this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
	}
	
	
	//methode pour generer une cle de chiffrement
	public SecretKey genererCle() throws NoSuchAlgorithmException {
		KeyGenerator keyGen;
		keyGen = KeyGenerator.getInstance("DESede");
		keyGen.init(168);
		SecretKey cle = keyGen.generateKey();
		return cle;

	}








}