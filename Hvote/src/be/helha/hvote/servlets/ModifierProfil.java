package be.helha.hvote.servlets;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import be.helha.hvote.domaine.Candidat;
import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.exceptions.ArgumentInvalideException;
import be.helha.hvote.usecases.GestionElecteur;


/**
 * Servlet implementation class Profil
 */

@WebServlet(urlPatterns = "/modifierProfil.html", name = "modifierProfil.html")
public class ModifierProfil extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String VIEW = "/WEB-INF/jsp/modifierProfil.jsp";

	@EJB
	private GestionElecteur gestionElecteur;
	
	public ModifierProfil() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		
		synchronized (session) {
			Electeur electeur= (Electeur) session.getAttribute(Constantes.ATT_USER);
			request.setAttribute(Constantes.ATT_USER, electeur);
			request.setAttribute(Constantes.ATT_TITRE, "Profil");
			this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		Candidat candidat = null;
		String ban =request.getParameter(Constantes.ATT_BAN);
		String photo = request.getParameter(Constantes.ATT_PHOTO);
		String message = "";
		Electeur electeur= (Electeur) session.getAttribute(Constantes.ATT_USER);
		String nom = electeur.getCandidat().getNom(); 
		String prenom = electeur.getCandidat().getPrenom();
		
		try {
			 candidat = new Candidat(nom,prenom,photo,ban);
		} catch (ArgumentInvalideException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		if(!ban.isEmpty() && !photo.isEmpty() ) {

			
			electeur.setCandidat(candidat);
			this.gestionElecteur.update(electeur);
			message ="Modification effectu�e";
			
		} 	
		if(ban.isEmpty() && photo.isEmpty() ) {

			
			electeur.setCandidat(candidat);
			this.gestionElecteur.update(electeur);
			message ="Modification effectu�e";
			
		} 
		request.setAttribute(Constantes.ATT_USER, electeur);
		request.setAttribute(Constantes.ATT_MESSAGE, message);
		this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
	}

}