package be.helha.hvote.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import be.helha.hvote.domaine.Electeur;

@SuppressWarnings("serial")
@WebServlet(urlPatterns="/accueil.html", name="accueil.html")
public class Accueil extends HttpServlet {
	private static final String VIEW = "/WEB-INF/jsp/index.jsp";
	
	public Accueil() {
        super();
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		
		synchronized (session) {
			Electeur electeur = (Electeur) session.getAttribute(Constantes.ATT_USER);
			request.setAttribute(Constantes.ATT_USER, electeur);
			request.setAttribute(Constantes.ATT_TITRE, "Accueil");
			this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		
		synchronized (session) {
			Electeur electeur = (Electeur) session.getAttribute(Constantes.ATT_USER);
			session.removeAttribute(Constantes.ATT_USER);
			request.setAttribute(Constantes.ATT_TITRE, "Accueil");
			this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
		}
	}
}