package be.helha.hvote.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.usecases.GestionElection;

/**
 * Servlet implementation class AffichageResults
 */
@WebServlet(urlPatterns="/resultats.html", name="resultats.html")
public class AffichageResults extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String VIEW = "/WEB-INF/jsp/resultats.jsp"; 
	@EJB
	private GestionElection gestionElection;
	
	
   
    public AffichageResults() {
        super();
       
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(true);
		String message = "";
		
		synchronized (session) {
			Electeur electeur = (Electeur) session.getAttribute(Constantes.ATT_USER);
			/*if(electeur != null && electeur.isConnecte() ) {*/
				
				//recup les valeurs des votes
				//Modifier les lignes qui suivent
				
				Map<String, String> dataList= new HashMap<String, String>();
				dataList.put("Candidat1", "17");
				dataList.put("Candidat2", "99");
				dataList.put("Candidat3", "98");
				dataList.put("Candidat4", "95");
				dataList.put("Candidat5", "85");
				dataList.put("Candidat6", "77");
				dataList.put("Abstentions", "3");
				
				request.setAttribute(Constantes.ATT_BLOCKCHAIN2, dataList);
			}
		//}
		
		request.setAttribute(Constantes.ATT_MESSAGE, message);
		request.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
