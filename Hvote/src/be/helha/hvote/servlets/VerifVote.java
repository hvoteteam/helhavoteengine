package be.helha.hvote.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import be.helha.hvote.domaine.Block;
import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.domaine.Election;
import be.helha.hvote.usecases.GestionElection;

/**
 * Servlet implementation class VerifVote
 */
@WebServlet(urlPatterns="/verificationVote.html", name="verificationVote.html")
public class VerifVote extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String VIEW = "/WEB-INF/jsp/verificationVote.jsp"; 
	@EJB
	private GestionElection gestionElection;
	
	
	
   
    public VerifVote() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		String message = "";
		
		synchronized (session) {
			Electeur electeur = (Electeur) session.getAttribute(Constantes.ATT_USER);
			if(electeur != null && electeur.isConnecte()) {
				
				//recup les valeurs de la blockchain
				
				/*
				String sujet = (String) session.getAttribute(Constantes.ATT_ELECTION_SESS);
				System.out.println("sujet: "+sujet);
				Election electio = this.gestionElection.rechercherElection(sujet);
				
				Election elecCharge = this.gestionElection.chargerChain(electio);
				List<Block> listBlock = elecCharge.getBlockchain();
				
				for(Block b : listBlock) {
					
				}
				
				System.out.println(listBlock);
				request.setAttribute(Constantes.ATT_BLOCKCHAIN, listBlock);*/
				
				
				
				
				
				//remplacer les lignes qui suivent
				/*List<List<String>> chain = new ArrayList<List<String>>();
				for(int i=0; i<5; i++) {
					
					List<String> block = new ArrayList<String>();
					String hashPrev = "hashPrev"+i;
					String hash = "hash"+i;
					String data = "data"+i;
					block.add(hashPrev);
					block.add(hash);
					block.add(data);
					chain.add(block);
				}
				System.out.println(chain);
				request.setAttribute(Constantes.ATT_BLOCKCHAIN, chain);*/
				
			}
		}
		
		request.setAttribute(Constantes.ATT_MESSAGE, message);
		request.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}