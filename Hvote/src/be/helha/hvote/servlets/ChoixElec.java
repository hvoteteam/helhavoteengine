package be.helha.hvote.servlets;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import be.helha.hvote.domaine.Block;
import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.domaine.Election;
import be.helha.hvote.domaine.Programme;
import be.helha.hvote.exceptions.ArgumentInvalideException;
import be.helha.hvote.usecases.GestionElecteur;
import be.helha.hvote.usecases.GestionElection;
import be.helha.hvote.usecases.GestionProgramme;

/**
 * Servlet implementation class ChoixElec
 */

@WebServlet(urlPatterns="/choixElec.html", name="choixElec.html")
public class ChoixElec extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String VIEW = "/WEB-INF/jsp/choixElec.jsp";
	@EJB
	private GestionElection gestionElection;
	@EJB
	private GestionElecteur gestionElecteur;
	@EJB
	private GestionProgramme gestionProg;
       
	public ChoixElec() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		///Check si user est conecte et peut donc acceder a la page
		HttpSession session = request.getSession(true);
		String message = "";
		
		synchronized (session) {
			Electeur electeur = (Electeur) session.getAttribute(Constantes.ATT_USER);
			if(electeur != null && electeur.isConnecte()) {
				
				
				
				//liste de toutes les elections
				List<Election> listeElection = gestionElection.listerTout();
				request.setAttribute(Constantes.ATT_LISTE_ALL_ELECTIONS, listeElection);
				
				//pour savoir si admin ou pas et permettre de modifier
				List<String> listIsAdmin = new ArrayList<String>();
				for(Election elec : listeElection) {
					if(elec.getAdmin().getPseudoMail().equals(electeur.getPseudoMail())) {
						listIsAdmin.add("yes");
					}
					else {
						listIsAdmin.add("no");
					}
				}
				request.setAttribute(Constantes.ATT_LIST_ADMIN, listIsAdmin);
				
				//savoir si candidat et permettre modifier prog
				List<String> listIsCand = new ArrayList<String>();
				for(Election e : listeElection) {
					int trouve = 0;
					e = this.gestionElection.chargerTout(e);
					List<Programme> listProg = e.getProgrammes();
					for(Programme p : listProg) {
						if(p.getCandidat().getPseudoMail().equals(electeur.getPseudoMail())) {
							trouve = 1;
						}
					}
					if(trouve == 1) {
						listIsCand.add("yes");
					}
					else {
						listIsCand.add("no");
					}
				}
				request.setAttribute(Constantes.ATT_LIST_ISCAND, listIsCand);
				
	
				request.setAttribute(Constantes.ATT_USER, electeur);
				request.setAttribute(Constantes.ATT_MESSAGE, message);
			
				VIEW = "/WEB-INF/jsp/choixElec.jsp";
				this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
				
			} 
			else {
				
				electeur = null;
				session.removeAttribute(Constantes.ATT_USER);
				
				response.sendRedirect( request.getContextPath() + "/accueil.html" );
			}
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		String election = request.getParameter(Constantes.ATT_ELECTION); 
		
		Electeur user = (Electeur) session.getAttribute(Constantes.ATT_USER);
		String message = "";
		Election found = null;
		int readyToVote = 0;
		Election progCharges = null;
		List<Programme> listP = null;
		Map<String, Integer> dataList;
		int bestP = Integer.MIN_VALUE;
		int secondP = Integer.MIN_VALUE;
		Election elecsaved = null;
		int checkT1 =0;
		
		String btnCh = request.getParameter("btnChoose");
		String btnMo = request.getParameter("btnModif");
		String btnPr = request.getParameter("btnProg");
		
		synchronized (session) {
			
			if(btnPr!=null) {		//Choisi btn du prog
				request.setAttribute(Constantes.ATT_USER, user);
				request.setAttribute(Constantes.ATT_ELECTION, election);
				request.setAttribute(Constantes.ATT_MESSAGE, message);
				
				VIEW = "/WEB-INF/jsp/modifierProgramme.jsp";
				this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
				
			}
			else {
				
			if(btnMo!=null) {		//Choisi btn de modif elec
				request.setAttribute(Constantes.ATT_USER, user);
				request.setAttribute(Constantes.ATT_ELECTION, election);
				request.setAttribute(Constantes.ATT_MESSAGE, message);
				
				VIEW = "/WEB-INF/jsp/modifierElection.jsp";
				this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
		
			}
			else {					//Choisi btn de vote
				

			
			if(election.isEmpty() || election==null) { //Rien choisi? Normalement on peut pas arriver ici
				message = "Veuillez choisir une �lection";
				request.setAttribute(Constantes.ATT_USER, user);
				request.setAttribute(Constantes.ATT_MESSAGE, message);
				this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
			}
			else {
				found = this.gestionElection.rechercherElection(election);
				
				//verIF dates depassees
				
				if(found.getNbTours()==1) { 		//1tour
					Date deb = found.getStartDate1();
					Date fin = found.getEndDate1();
					Date now = new Date();
					if(now.after(fin)) {			//date de fin depassee
						//renvoyer vers les resultats
					
						progCharges = this.gestionElection.chargerTout(found);
						listP = progCharges.getProgrammes();
						dataList = new HashMap<String, Integer>();
						for(Programme p : listP) {
							dataList.put(p.getCandidat().getPseudoMail(), p.getCptVote());
						}
						request.setAttribute(Constantes.ATT_BLOCKCHAIN2, dataList);
						request.setAttribute(Constantes.ATT_ELECTION, election);
						request.setAttribute(Constantes.ATT_USER, user);
						request.setAttribute(Constantes.ATT_MESSAGE, message);
						VIEW = "/WEB-INF/jsp/resultats.jsp";
						this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
						
					}
					else {
						if(now.before(deb)) {			//pas encore commence
							
							message = "Cette �lection n'a pas encore commenc�.";
							//liste de toutes les elections
							List<Election> listeElection = gestionElection.listerTout();
							request.setAttribute(Constantes.ATT_LISTE_ALL_ELECTIONS, listeElection);
							
							//pour savoir si admin ou pas et permettre de modifier
							List<String> listIsAdmin = new ArrayList<String>();
							for(Election elec : listeElection) {
								if(elec.getAdmin().getPseudoMail().equals(user.getPseudoMail())) {
									listIsAdmin.add("yes");
								}
								else {
									listIsAdmin.add("no");
								}
							}
							
							request.setAttribute(Constantes.ATT_LIST_ADMIN, listIsAdmin);
							
							//savoir si candidat et permettre modifier prog
							List<String> listIsCand = new ArrayList<String>();
							for(Election e : listeElection) {
								int trouve = 0;
								e = this.gestionElection.chargerTout(e);
								List<Programme> listProg = e.getProgrammes();
								for(Programme p : listProg) {
									if(p.getCandidat().getPseudoMail().equals(user.getPseudoMail())) {
										trouve = 1;
									}
								}
								if(trouve == 1) {
									listIsCand.add("yes");
								}
								else {
									listIsCand.add("no");
								}
							}
							request.setAttribute(Constantes.ATT_LIST_ISCAND, listIsCand);
							
							request.setAttribute(Constantes.ATT_USER, user);
							request.setAttribute(Constantes.ATT_MESSAGE, message);
						
							VIEW = "/WEB-INF/jsp/choixElec.jsp";
							this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);		//renvoie au choix avec message
							
						}
						else {
							//else on fait rien et on poursuit le traitement en verifiant si deja vote
							readyToVote=1;
						}
					}

					
				}
				else {
				if(found.getNbTours()==2) {								//2tours
					Date fin1 = found.getEndDate1();
					Date fin2 = found.getEndDate2();
					Date deb1 = found.getStartDate1();
					Date deb2 = found.getStartDate2();
					Date now = new Date();
					
					if(now.before(deb1)) {							//tour 1 pas encore commence
						message = "Cette �lection n'a pas encore commenc�.";
						//liste de toutes les elections
						List<Election> listeElection = gestionElection.listerTout();
						request.setAttribute(Constantes.ATT_LISTE_ALL_ELECTIONS, listeElection);
						
						//pour savoir si admin ou pas et permettre de modifier
						List<String> listIsAdmin = new ArrayList<String>();
						for(Election elec : listeElection) {
							if(elec.getAdmin().getPseudoMail().equals(user.getPseudoMail())) {
								listIsAdmin.add("yes");
							}
							else {
								listIsAdmin.add("no");
							}
						}
					
						request.setAttribute(Constantes.ATT_LIST_ADMIN, listIsAdmin);
						
						//savoir si candidat et permettre modifier prog
						List<String> listIsCand = new ArrayList<String>();
						for(Election e : listeElection) {
							int trouve = 0;
							e = this.gestionElection.chargerTout(e);
							List<Programme> listProg = e.getProgrammes();
							for(Programme p : listProg) {
								if(p.getCandidat().getPseudoMail().equals(user.getPseudoMail())) {
									trouve = 1;
								}
							}
							if(trouve == 1) {
								listIsCand.add("yes");
							}
							else {
								listIsCand.add("no");
							}
						}
						request.setAttribute(Constantes.ATT_LIST_ISCAND, listIsCand);
						
						request.setAttribute(Constantes.ATT_USER, user);
						request.setAttribute(Constantes.ATT_MESSAGE, message);
					
						VIEW = "/WEB-INF/jsp/choixElec.jsp";
						this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);		//renvoie au choix avec message
					}
					else {
						if(!(now.before(deb1) || now.after(fin1))) {		//on est au tour 1 pour le moment car pas avant debut ni apres fin du t1
							
							//continue traitement et verifie si deja vote au t1
							readyToVote=1;
						}
						else {
							if(!(now.before(fin1) || now.after(deb2))) {	//on est apres t1 mais pas encore t2
								//renvoyer vers resultats du t1
								//garder 2 candidats
								
								
								int first = Integer.MIN_VALUE; 
								int second = Integer.MIN_VALUE;
								
								progCharges = this.gestionElection.chargerTout(found);
								listP = progCharges.getProgrammes();
								dataList = new HashMap<String, Integer>();
								for(Programme p : listP) {
									dataList.put(p.getCandidat().getPseudoMail(), p.getCptVote());
									
									if(p.getCptVote() > first) {
										secondP = bestP;
							            bestP = p.getId();
									}
									else {
										if(p.getCptVote() > second) {
											secondP = p.getId();
										}
									}
									
								}
								//bestP = id du best prog et secondP = id du second prog
								
								Programme prog1 = this.gestionProg.rechercherProg(bestP);
								Programme prog2 = this.gestionProg.rechercherProg(secondP);
								
								Election elecT2_1 = this.gestionElection.sendToT2(found, prog1);
								Election elecT2_2 = this.gestionElection.sendToT2(elecT2_1, prog2);
								
								
								
								request.setAttribute(Constantes.ATT_BLOCKCHAIN2, dataList);
								request.setAttribute(Constantes.ATT_ELECTION, election);
								request.setAttribute(Constantes.ATT_USER, user);
								request.setAttribute(Constantes.ATT_MESSAGE, message);
								
								
								VIEW = "/WEB-INF/jsp/resultats.jsp";
								this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
							}
							else {
								if(!(now.before(deb2) || now.after(fin2))) {		//on est au 2eme tour mtn
									found = this.gestionElection.nextTurn(found);
									found = this.gestionElection.sauver(found);	//marque bien le changement de tour
									
									//continue traitement et verifie si deja vote au t2
									//verifier si vote ou pas au t1 pour plus tard
									readyToVote=1;
									checkT1=1;
								}
								else {
									if(now.after(fin2)) {							//apres tour 2
										//renvoyer vers resultats
										
										progCharges = this.gestionElection.chargerTout(found);
										listP = progCharges.getProgrammes();
										dataList = new HashMap<String, Integer>();
										for(Programme p : listP) {
											if(p.getGoToT2()==1) {
												dataList.put(p.getCandidat().getPseudoMail(), p.getCptVoteT2());	//affichage resultats t2
											}
											
										}
										request.setAttribute(Constantes.ATT_BLOCKCHAIN2, dataList);
										request.setAttribute(Constantes.ATT_ELECTION, election);
										request.setAttribute(Constantes.ATT_USER, user);
										request.setAttribute(Constantes.ATT_MESSAGE, message);
										
										VIEW = "/WEB-INF/jsp/resultats.jsp";
										this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
									}
								}
							}
						}
					}

					
				}//fin else 2tours
				}//fin else 1tour
			}//else les valeurs sont bonnes	
				
			//verif si peut voter ou pas si abstenu au tour 1
			if(checkT1==1) {
				if(found.getTourActuel()==2) {
					if(found.getCantVoteT2IfNoT1()==1) {	//cas avec restrictions
						if(this.gestionElection.hasVotedT1(found, user)==false) {
							readyToVote=0;
							message = "Vous ne pouvez plus voter dans cette �lection car vous vous �tes abstenu au tour 1.";
							//liste de toutes les elections
							List<Election> listeElection = gestionElection.listerTout();
							request.setAttribute(Constantes.ATT_LISTE_ALL_ELECTIONS, listeElection);
							
							//pour savoir si admin ou pas et permettre de modifier
							List<String> listIsAdmin = new ArrayList<String>();
							for(Election elec : listeElection) {
								if(elec.getAdmin().getPseudoMail().equals(user.getPseudoMail())) {
									listIsAdmin.add("yes");
								}
								else {
									listIsAdmin.add("no");
								}
							}
							
							request.setAttribute(Constantes.ATT_LIST_ADMIN, listIsAdmin);
							
							//savoir si candidat et permettre modifier prog
							List<String> listIsCand = new ArrayList<String>();
							for(Election e : listeElection) {
								int trouve = 0;
								e = this.gestionElection.chargerTout(e);
								List<Programme> listProg = e.getProgrammes();
								for(Programme p : listProg) {
									if(p.getCandidat().getPseudoMail().equals(user.getPseudoMail())) {
										trouve = 1;
									}
								}
								if(trouve == 1) {
									listIsCand.add("yes");
								}
								else {
									listIsCand.add("no");
								}
							}
							request.setAttribute(Constantes.ATT_LIST_ISCAND, listIsCand);
							
							request.setAttribute(Constantes.ATT_USER, user);
							request.setAttribute(Constantes.ATT_MESSAGE, message);
						
							VIEW = "/WEB-INF/jsp/choixElec.jsp";
							this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);		//renvoie au choix avec message
						}
					}
				}
			}
			
			
				
				// verif deja vote ou pas si dates bonnes
			if(readyToVote==1) {
				if(this.gestionElection.hasAlreadyVote(found, user)==true) {					//A deja vote
					
					message = "Vous avez d�j� vot� pour l'�lection "+found.getSujet()+".";
					String raw = "";
					
					Election electio = this.gestionElection.rechercherElection(election);
					
					Election elecCharge = this.gestionElection.chargerChain(electio);
					List<Block> listBlock = elecCharge.getBlockchain();
					List<String> listToDisplay = new ArrayList<String>();
					
					for(Block b : listBlock) {
						try {
							raw = b.decrypter(b.getData(), b.getCle());
						} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
								| IllegalBlockSizeException | BadPaddingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						String[] parts = raw.split(" ", -1);
						
						if(parts[1].equals(user.getPseudoMail())) {
							listToDisplay.add(raw);
						}
						else {
							listToDisplay.add(b.getData().toString());
						}
					}
					
					
					request.setAttribute(Constantes.ATT_BLOCKCHAIN, listToDisplay);
					
					request.setAttribute(Constantes.ATT_ELECTION, election);
					request.setAttribute(Constantes.ATT_USER, user);
					request.setAttribute(Constantes.ATT_MESSAGE, message);
					VIEW = "/WEB-INF/jsp/verificationVote.jsp";
				
					this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
				}
				else {
				if(this.gestionElection.hasAlreadyVote(found, user)==false) {					// a pas vote
					

					Election elecProg = this.gestionElection.chargerTout(found);
					List<Programme> listProg = elecProg.getProgrammes();
					
					List<Electeur> listCand = new ArrayList<Electeur>();
					
					if(elecProg.getTourActuel()==1) {
						request.setAttribute(Constantes.ATT_LISTE_ALL_PROGS, listProg);		//tous les progs
						
						for(Programme prog : listProg){
							Electeur cand = prog.getCandidat();
							listCand.add(cand);
						}
						request.setAttribute(Constantes.ATT_LISTE_ALL_CANDIDATS, listCand);	//tous les candid
					}
					else {
						if(elecProg.getTourActuel()==2) {
							List<Programme> listT2 = new ArrayList<Programme>();
							for(Programme p : listProg) {
								if(p.getGoToT2()==1) {
									listT2.add(p);
									listCand.add(p.getCandidat());
								}
							}
							request.setAttribute(Constantes.ATT_LISTE_ALL_PROGS, listT2);	//seulement prog t2
							request.setAttribute(Constantes.ATT_LISTE_ALL_CANDIDATS, listCand);	//candid du t2
						}
					}
					
					
					
					
					
					
					
					request.setAttribute(Constantes.ATT_ELECTION, election);
					request.setAttribute(Constantes.ATT_ELECTION_COULEUR, found.getTheme());
					
					request.setAttribute(Constantes.ATT_ELECTION_URL_BAN, found.getBanniere());
					request.setAttribute(Constantes.ATT_USER, user.getPseudoMail());
					request.setAttribute(Constantes.ATT_MESSAGE, message);
					VIEW = "/WEB-INF/jsp/vote.jsp";
					this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
				}
				}
			}
			
		}
			
		}
		
		}
	
		}

	}