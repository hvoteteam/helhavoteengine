package be.helha.hvote.servlets;

public interface Constantes {
	String PARAM_LOGIN = "login";
	String PARAM_MDP = "mdp";
	String PARAM_MDP2 = "mdp2";
	
	String ATT_LOGIN = "login";
	String ATT_CONN = "conn";
	String ATT_PSWD = "pswd";
	String ATT_PSWD_2 = "pswd2";
	String ATT_USER = "electeur";
	String ATT_MESSAGE = "message";
	String ATT_TITRE = "titre";
	
	String ATT_NOM ="nom"; 
	String ATT_PRENOM ="prenom";
	String ATT_BAN ="ban"; 
	String ATT_PHOTO ="photo";
	
	String ATT_SUJET = "sujet";
	String ATT_ADDELECT = "addElect";//election
	String ATT_ADDCANDIDAT = "addCandidat";
	String ATT_LISTE_ALL_ELECTEURS = "listeAllElecteurs";
	String ATT_LISTE_ALL_CANDIDATS = "listeAllCandidats";
	String ATT_LISTE_ALL_PROGS = "listeAllProgs";
	String ATT_LISTE_ALL_ELECTIONS = "listeAllElections";
	String ATT_ELECTION = "election";
	String ATT_VOTE = "vote";
	String ATT_PROG_CHOISI = "choix";
	
	String ATT_RB_NBTOURS = "nbtours";
	String ATT_DATE_DEB1 = "deb1";
	String ATT_DATE_DEB2 = "deb2";
	String ATT_DATE_FIN1 = "fin1";
	String ATT_DATE_FIN2 = "fin2";
	String ATT_CB_DROIT = "cbdroit";
	
	String ATT_BLOCKCHAIN = "blockchain";
	String ATT_BLOCKCHAIN2 = "blockchain2";
	String ATT_ELECTION_SESS = "electionsess";
	

	String ATT_ELECTION_URL_BAN = "electionUrlBan";
	String ATT_ELECTION_COULEUR = "electionCouleur";
	
	
	String VUE_UPLOAD_IMAGE = "ChargerFichierImage";
	String VUE_AFFICHER_IMAGE = "AfficherFichierImage";

	String FICHIER_IMAGE = "uneImage.PNG";
	
	String PARAM_NOM_IMAGE = "nomImage";
	
	boolean REPERTOIRE_EXTERNE_CHOISI = true; // =false si relative path , =true si absolute path
	String REPERTOIRE_UPLOADS = "uploads/"; // relative path
	String REPERTOIRE_EXTERNE = "d:/temp/"; // absolute path
	
	String ATT_LIST_ADMIN = "listeIsAdmin";
	String ATT_LIST_ISCAND = "listeIsCand";
	
	String ATT_TEXT_PROG = "textProg";
	
}