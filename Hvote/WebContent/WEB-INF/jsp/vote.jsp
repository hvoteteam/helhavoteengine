<%@ page language="java" contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE html>
<html style="height: 100%;">
<head>
<c:url var="urlStyle" value="/css/style.css"/>
<link rel="stylesheet" href="${urlStyle}" type="text/css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<title>Creation Election</title>
</head>
<body style="height: 100%;">

	<div class="row h-100" style="background-image: url('https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/BuKKyFq/videoblocks-flat-style-animation-of-a-hand-casting-vote-in-the-ballot-box_ha6aboftx_thumbnail-full02.png'); background-repeat: no-repeat; background-size: cover; background-attachment: fixed; background-position: center center;">
		<div class="card card border-primary text-center mx-auto my-auto" style="width: 50%; height: auto; background-color: ${electionCouleur}">
			<c:if test="${ not empty electionUrlBan }">
			<div class="col-md-12">
					<img src="${electionUrlBan}" class="card-img-top img-fluid h-25 ">
			</div>
			</c:if>
			<div class="card-body">
				<h5 class="card-title font-weight-bold py-1">${election}</h5>
				
				<c:url var="urlVote" value="/vote.html" />
				
				
				<div class="form-group">
					<c:forEach items="${ listeAllCandidats }" var="cands" varStatus="vs">
					<form method="post" action="${urlVote}"> 
						<div class="card mx-auto my-auto" style="width: 75%;">
  							<div class="card-body">
							    <h5 class="card-title">${cands.pseudoMail}</h5>
							    <p>${listeAllProgs[vs.index].texteProgramme}</p>
							    <input type="hidden" name="electeur" id="electeur" value="${electeur}" class="form-control">
							    <input type="hidden" name="election" id="election" value="${election}" class="form-control">
							    <input type="hidden" name="choix" id="choix" value="${listeAllProgs[vs.index].id}" class="form-control">
							    <input type="submit" value="Voter pour moi" class="btn btn-primary pt-1">
							    
							    
									
							 </div>
						</div>
					</form>
					</c:forEach>	
					
				</div>  
				  
				
				
				<c:if test="${ not empty message }">
				<hr>
				<p class="text-warning font-italic">${ message }</p>
				</c:if>
				
				<hr>
				<c:url var="accueil" value="/accueil.html"/> 
				<a href="${accueil}" class="card-link col-3 btn btn-primary">Accueil</a>
				
			</div>
		</div>
	</div>

</body>
</html>