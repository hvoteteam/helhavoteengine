<%@ page language="java" contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE html  style="height: 100%;">
<html>
<head>
<c:url var="urlStyle" value="/css/style.css"/>
<link rel="stylesheet" href="${urlStyle}" type="text/css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<title>${ titre }</title>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">

//Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {
    'packages' : [ 'corechart' ]
});
// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawChart);
// Callback that creates and populates a data table,
// instantiates the  chart, passes in the data and
// draws it.
function drawChart() {
    // Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Candidats');
    data.addColumn('number', 'Nombre d\'�lecteurs');
    data.addRows([
    	<c:forEach var="items" items="${blockchain2}"> 
    		[ '${items.key}', ${items.value} ],          
      	</c:forEach> 

                ]);
    data.sort([{column: 1, desc: true}]);
 	// Set chart options
    var options = {legend: 'none',
    		bar: {groupWidth: "75%"},
    	
              };
    
    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}

</script>
</head>
<body style="height: 100%;">
	<div class="row h-100" style="background-image: url('https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/BuKKyFq/videoblocks-flat-style-animation-of-a-hand-casting-vote-in-the-ballot-box_ha6aboftx_thumbnail-full02.png'); background-repeat: no-repeat; background-size: cover; height: 100%; background-position: center center;">
		<div class="card card border-primary text-center mx-auto my-auto" style="width: 75%; height: auto;">
			<div class="card-body">
				<h5 class="card-title font-weight-bold py-1">R�sultats de l'�lection ${election}</h5>
				
				<div id="chart_div"></div>
				
				
				<c:if test="${ not empty message }">
				<hr>
				<p class="text-warning font-italic">${ message }</p>
				</c:if>
				
				<hr>
				<c:url var="accueil" value="/accueil.html"/> 
				<a href="${accueil}" class="card-link col-3">Accueil</a>
			</div>
		</div>
	</div>

</body>
</html>
