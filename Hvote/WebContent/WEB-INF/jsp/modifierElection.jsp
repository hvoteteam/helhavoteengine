<%@ page language="java" contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html style="height: 100%;">
<head>
<c:url var="urlStyle" value="/css/style.css"/>
<link rel="stylesheet" href="${urlStyle}" type="text/css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<title>${ titre }</title>
</head>
<body style="height: 100%;">
	
	<div class="row h-100" style="background-image: url('https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/BuKKyFq/videoblocks-flat-style-animation-of-a-hand-casting-vote-in-the-ballot-box_ha6aboftx_thumbnail-full02.png'); background-repeat: no-repeat; background-size: cover; background-position: center center;  background-attachment: fixed;">
		<div class="card card border-primary text-center mx-auto my-auto" style="width: 50%; height: auto;">
			<div class="card-body">
				<h5 class="card-title font-weight-bold py-1">Modification de l'�lection ${election}</h5>
				
				<c:url var="urlModification" value="/modifElection.html" />
				<form method="post" action="${urlModification}">
				
				<div class="form-group">	
					<label for="addBan" class="text-left">Choisir une banni�re</label>
					<input type="text" name="electionUrlBan" id="electionUrlBan" class="form-control"/>
					<small>Attention, ne pas compl�ter le champ banni�re revient � la supprimer</small>	
				</div>
				<br>
				<div class="form-group">
					<label for="addColor">Choisir une couleur</label>
    				<input type="color" id="electionCouleur" name="electionCouleur" value="#ffffff">
				</div>
        
        		<input type="hidden" name="election" id="election" value="${election}" class="form-control">
				<input type="hidden" name="electeur" id="electeur" value="${electeur}" class="form-control">
				
				<input type="submit" value="Sauver les changements" class="btn btn-primary pt-1">
				</form>
				
				<c:if test="${ not empty message }">
				<hr>
				<p class="text-warning font-italic">${ message }</p>
				</c:if>
				
				<hr>
				<c:url var="accueil" value="/accueil.html"/> 
				<a href="${accueil}" class="card-link col-3">Accueil</a>
				
			</div>
		</div>
	</div>

</body>
</html>