<%@ page language="java" contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html style="height: 100%;">
<head>
<c:url var="urlStyle" value="/css/style.css"/>
<link rel="stylesheet" href="${urlStyle}" type="text/css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<title>${ titre }</title>
</head>
<body style="height: 100%;">
	<div class="row h-100" style="background-image: url('https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/BuKKyFq/videoblocks-flat-style-animation-of-a-hand-casting-vote-in-the-ballot-box_ha6aboftx_thumbnail-full02.png'); background-repeat: no-repeat; background-size: cover; height: 100%; background-position: center center;">
		<div class="card card border-primary text-center mx-auto my-auto" style="width: 50%; height: auto;">
		<div class="card-body">
			<h5 class="card-title font-weight-bold py-1">Modifier votre profil</h5>
			<c:url var="urlCible" value="/modifierProfil.html" />
			<form method="post" action="${urlCible}">
				<div class="form-group">
					<label for="login" class="text-left">Banni�re</label> <input
						type="text" name="ban" id="ban" class="form-control"
						value="${electeur.candidat.banniere}">
				</div>
				<div class="form-group">
					<label for="pswd" class="text-left">Photo</label> <input
						type="text" name="photo" id="photo" class="form-control"
						value="${electeur.candidat.photo}">
				</div>
				<input type="submit" value="Confirmer" class="btn btn-primary pt-1">
			</form>
			<c:if test="${not  empty message }">
				<hr>
				<p class="text-warning font-italic">${ message }</p>
			</c:if>
			<hr>
			<c:url var="profil" value="/profil.html" />
			<a href="${profil}" class="card-link col-3">Profil</a>
			<div class="justify-content-around"></div>
		</div>
	</div>
</body>
</html>
