<%@ page language="java" contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE html  style="height: 100%;">
<html>
<head>
<c:url var="urlStyle" value="/css/style.css"/>
<link rel="stylesheet" href="${urlStyle}" type="text/css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<title>${ titre }</title>
</head>
<body style="height: 100%;">
	<div class="row h-100" style="background-image: url('https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/BuKKyFq/videoblocks-flat-style-animation-of-a-hand-casting-vote-in-the-ballot-box_ha6aboftx_thumbnail-full02.png'); background-repeat: no-repeat; background-size: cover; height: 100%; background-position: center center;">
		<div class="card card border-primary text-center mx-auto my-auto" style="width: 75%; height: auto;">
			<div class="card-body">
				<h5 class="card-title font-weight-bold py-1">Liste des votes de l'�lection ${election}</h5>
				<p><small>Les votes sont chiffr�s afin de garder l'identit� et le choix de chacun secret</small></p>
				
				<c:choose>
					<c:when test="${empty blockchain}">
						<p class="erreur">Il n'y a pas encore de votes</p>
					</c:when>
					<c:otherwise>
					
					<table class="table text-center mx-auto my-auto" style="width: 75%; height: auto;">
					<thead class="thead-light">
					   <tr>
					     <th scope="col">Num�ro</th>
					     <th scope="col">Donn�es</th>
					   </tr>
					 </thead>
					<c:forEach items="${blockchain}" var="block" varStatus="vs">
					 <tbody>
					    <tr>
					     <th scope="row">${ vs.index }</th>
					     <td>${ block }</td>
					   </tr>
					 </tbody>
					</c:forEach>
					</table>
				
					</c:otherwise>
				</c:choose>
				
				<c:if test="${ not empty message }">
				<hr>
				<p class="text-warning font-italic">${ message }</p>
				</c:if>
				
				<hr>
				<c:url var="accueil" value="/accueil.html"/> 
				<a href="${accueil}" class="card-link col-3">Accueil</a>
				
			
			</div>
		</div>
	</div>
</body>
</html>