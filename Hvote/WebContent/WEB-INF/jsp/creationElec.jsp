<%@ page language="java" contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE html>
<html style="height: 100%;">
<head>
<c:url var="urlStyle" value="/css/style.css"/>
<link rel="stylesheet" href="${urlStyle}" type="text/css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<title>Creation Election</title>
</head>
<body style="height: 100%;">

	<div style="background-image: url('https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/BuKKyFq/videoblocks-flat-style-animation-of-a-hand-casting-vote-in-the-ballot-box_ha6aboftx_thumbnail-full02.png'); background-repeat: no-repeat; background-size: cover; background-position: center center;  background-attachment: fixed;">
		<div class="card card border-primary text-center mx-auto my-auto" style="width: 50%; height: auto;">
			<div class="card-body">
				<h5 class="card-title font-weight-bold py-1">Cr�ation de l'�lection</h5>
				
				<c:url var="urlCreation" value="/creationElec.html" />
				<form method="post" action="${urlCreation}">
				
				<div class="form-group">
					<label for="sujet" class="text-left">Sujet</label>
					<input type="text" name="sujet" id="sujet" class="form-control" autocomplete="off">
				</div> 
				
				<div class="form-group">
					<label for="addElect" class="text-left">D�signer des �lecteurs</label>
					<c:choose>
					<c:when test="${request.listeAllElecteurs.isEmpty()}">
						<p class="erreur">Il n'y a aucun �lecteur potentiel</p>
					</c:when>
					<c:otherwise>
						<input type="email" name="addElect" id="addElect" class="form-control" list="emailsL" autocomplete="off" multiple>
						<datalist id="emailsL">
							<c:forEach items="${listeAllElecteurs}" var="electeur">
							<option value="${electeur.pseudoMail}">${electeur.pseudoMail}</option>
							</c:forEach>		    
						</datalist>
					</c:otherwise>
					</c:choose>
				</div> 
				
				<div class="form-group">	
					<label for="addCandidat" class="text-left">D�signer des candidats</label>
                	<c:choose>
					<c:when test="${request.listeAllCandidats.isEmpty() }">
						<p class="erreur">Il n'y a aucun candidat potentiel</p>
					</c:when>
					<c:otherwise>
                		<input type="email" class="form-control" id="addCandidat" name="addCandidat" list="candL" autocomplete="off" multiple>
                 		<datalist id="candL">
                 			<c:forEach items="${listeAllCandidats}" var="cand" >
                 				<option value="${cand.pseudoMail}">
                 			</c:forEach>
                 		</datalist>
                 	</c:otherwise>
                 	</c:choose>
				</div> 
				   
				<div class="form-group">
					<label for="nbtours" class="text-left">D�finir nombre de tours</label>
					<br>
					<input type="radio" name="nbtours" value="1" checked="checked" onclick="document.getElementById('deb2').disabled = true; document.getElementById('fin2').disabled = true; document.getElementById('cbdroit').disabled = true;"> 1 tour     
					<input type="radio" name="nbtours" value="2" onclick="document.getElementById('deb2').disabled = false; document.getElementById('fin2').disabled = false; document.getElementById('cbdroit').disabled = false; "> 2 tours
					<br>
				
					<label for="deb1" class="text-left">D�but du premier tour</label>
					<input type="date" name="deb1" id="deb1" class="form-control">
					
					<label for="fin1" class="text-left">Fin du premier tour</label>
					<input type="date" name="fin1" id="fin1" class="form-control">
					
					<label for="deb2" class="text-left">D�but du deuxi�me tour</label>
					<input type="date" name="deb2" id="deb2" class="form-control" disabled="disabled">
					
					<label for="fin2" class="text-left">Fin du deuxi�me tour</label>
					<input type="date" name="fin2" id="fin2" class="form-control" disabled="disabled">
					
				</div> 
				
				<div class="form-group">
					<label for="cbdroit" class="text-left">R�action si abstention au premier tour</label>
					<input type="checkbox" name="cbdroit" id="cbdroit" class="form-control" disabled="disabled" value="nomore"> Retirer le droit de vote au tour 2
				</div>
				
				<div class="form-group">
					<label for="addBan" class="text-left">Choisir une banni�re</label>
					<input type="text" name="electionUrlBan" id="electionUrlBan" class="form-control"/>
				</div>
				
				<div class="form-group">
					<label for="addColor">Choisir une couleur</label>
    				<input type="color" id="electionCouleur" name="electionCouleur" value="#ffffff">
				</div>
				   
				           
				
				<input type="submit" value="Cr�er" class="btn btn-primary pt-1">
				</form>
				
				<c:if test="${ not empty message }">
				<hr>
				<p class="text-warning font-italic">${ message }</p>
				</c:if>
				
				<hr>
				<c:url var="accueil" value="/accueil.html"/> 
				<a href="${accueil}" class="card-link col-3">Accueil</a>
				
			</div>
		</div>
	</div>

</body>
</html>