<%@ page language="java" contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE html>
<html style="height: 100%;">
<head>
<c:url var="urlStyle" value="/css/style.css"/>
<link rel="stylesheet" href="${urlStyle}" type="text/css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<title>Choix Election</title>
</head>
<body style="height: 100%;">

	<div class="row min-vh-100" style="background-image: url('https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/BuKKyFq/videoblocks-flat-style-animation-of-a-hand-casting-vote-in-the-ballot-box_ha6aboftx_thumbnail-full02.png'); background-repeat: no-repeat; background-size: cover; background-attachment: fixed; background-position: center center;">
		<div class="card card border-primary text-center mx-auto my-auto mh-100" style="width: 50%; height: 100%;">
			<div class="card-body text-center">
				<h5 class="card-title font-weight-bold py-1">Choix de l'�lection</h5>
				
				<c:if test="${ not empty message }">
				<hr>
				<p class="text-warning font-italic">${ message }</p>
				<hr>
				</c:if>
				
				<div class="form-group">
					
					<c:choose>
					<c:when test="${listeAllElections.isEmpty()}">
						<p class="erreur">Aucune �lection disponible</p>
					</c:when>
					<c:otherwise>
					
						<c:forEach items="${listeAllElections}" var="elec" varStatus="vs">
						<c:url var="urlChoixElec" value="/choixElec.html" />
						<form method="post" action="${urlChoixElec}">
						
							<div class="card mx-auto my-auto" style="width: 75%;">
  								<div class="card-body">
							    	<h5 class="card-title">${elec.sujet}</h5>
							    	<input type="hidden" name="election" id="election" value="${elec.sujet}" class="form-control">
							    
							    	<input type="submit" name="btnChoose" value="Choisir" class="btn btn-primary pt-1">
							 		
									<c:if test="${listeIsAdmin[vs.index] eq 'yes'}">
							 			<input type="submit" name="btnModif" value="Modifier" class="btn btn-secondary pt-1">
							 		</c:if>
							 		
							 		<c:if test="${listeIsCand[vs.index] eq 'yes'}">
							 			<input type="submit" name="btnProg" value="Changer mon programme" class="btn btn-secondary pt-1">
							 		</c:if>
							 	</div>
							 	
							</div>
						</form>
						</c:forEach>
					
					
					</c:otherwise>
					</c:choose>
				</div> 
	
				
				<c:if test="${ not empty message }">
				<hr>
				<p class="text-warning font-italic">${ message }</p>
				</c:if>
				
				<hr>
				<c:url var="accueil" value="/accueil.html"/> 
				<a href="${accueil}" class="card-link col-3">Accueil</a>
				
			</div>
		</div>
	</div>

</body>
</html>