<%@ page language="java" contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html style="height: 100%;">
<head>
<c:url var="urlStyle" value="/css/style.css"/>
<link rel="stylesheet" href="${urlStyle}" type="text/css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<title>${ titre }</title>
</head>
<body style="height: 100%;">
	<div class="row h-100" style="background-image: url('https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/BuKKyFq/videoblocks-flat-style-animation-of-a-hand-casting-vote-in-the-ballot-box_ha6aboftx_thumbnail-full02.png'); background-repeat: no-repeat; background-size: cover; height: 100%; background-position: center center;">
	<c:choose>
	<c:when test="${ empty electeur || electeur.connecte == false }">
		
	<div class="card card border-primary text-center mx-auto my-auto" style="width: 50%; height: auto;">
  		<div class="card-body">
    		<h5 class="card-title font-weight-bold py-1">Connexion</h5>
    		<p class="card-text">Connectez-vous � votre compte. Si vous n'en poss�dez pas encore, cr�ez-en un.</p>
    		<div class="justify-content-around">
    		<c:url var="urlCreer" value="/inscription.html"/>
			<a href="${urlCreer}" class="card-link col-3">Cr�er un compte</a>
			
    		<c:url var="urlConnecter" value="/login.html"/>
			<a href="${urlConnecter}" class="card-link col-3">Se connecter</a>
			</div>
  		</div>
	</div>
	</c:when>
	
	<c:otherwise>
		<div class="card card border-primary text-center mx-auto my-auto" style="width: 50%; height: auto;">
			<div class="card-body">
				<h5 class="card-title font-weight-bold py-1">Bonjour</h5>
				<p class="text-warning font-italic">Vous �tes connect�  en tant que ${ electeur.pseudoMail }</p>
				
				<c:url var="urlCreerE" value="/creationElec.html"/>
				<a href="${urlCreerE}" class="card-link col-3">Cr�er une nouvelle �lection</a>
				<br>
				<c:url var="urlChoixE" value="/choixElec.html"/>
				<a href="${urlChoixE}" class="card-link col-3">Choisir une �lection</a>
				<br>
				<c:url var="urlProfil" value="/profil.html"/>
				<a href="${urlProfil}" class="card-link col-3">Acc�der au profil</a>
				<br>
				<br>
				<c:url var="indexURL" value="/accueil.html"/>
				<form method="POST" action="${ indexURL }">
					<button type="submit" name="logout" class="btn btn-primary pt-1">Se d�connecter</button>
				</form>
			</div>
		</div>
	</c:otherwise>
	</c:choose>
	</div>
	
</body>
</html>