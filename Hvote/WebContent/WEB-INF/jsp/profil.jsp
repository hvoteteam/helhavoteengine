<%@ page language="java" contentType="text/html;charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html style="height: 100%;">
<head>
<c:url var="urlStyle" value="/css/style.css" />
<link rel="stylesheet" href="${urlStyle}" type="text/css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<title>${titre}</title>
</head>
<body style="height: 100%;">
	<c:choose>
		<c:when
			test="${empty electeur.candidat.nom and empty electeur.candidat.prenom}">
			<div class="row h-100" style="background-image: url('https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/BuKKyFq/videoblocks-flat-style-animation-of-a-hand-casting-vote-in-the-ballot-box_ha6aboftx_thumbnail-full02.png'); background-repeat: no-repeat; background-size: cover; background-attachment: fixed; background-position: center center;">
				<div class="card card border-primary text-center mx-auto my-auto"
					style="width: 50%; height: auto;">
					<div class="card-body">
						<h5 class="card-title font-weight-bold py-1">Informations
							personnelles</h5>
						<c:url var="urlProfil" value="/profil.html" />
						<form method="post" action="${urlProfil}">
							<div class="form-group">
								<label for="login" class="text-left">Nom</label>
								<input type="text" name="nom" id="nom" class="form-control">
							</div>
							<div class="form-group">
								<label for="pswd" class="text-left">Pr�nom</label>
								<input type="text" name="prenom" id="prenom" class="form-control">
							</div>
							<input type="submit" value="Confirmer"
								class="btn btn-primary pt-1">
						</form>
						<c:if test="${ not empty message }">
							<hr>
							<p class="text-warning font-italic">${ message }</p>
						</c:if>
						<hr>
						<c:url var="accueil" value="/accueil.html" />
						<a href="${accueil}" class="card-link col-3">Accueil</a>
					</div>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="pt-5"
				style="background-image: url('https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/BuKKyFq/videoblocks-flat-style-animation-of-a-hand-casting-vote-in-the-ballot-box_ha6aboftx_thumbnail-full02.png'); background-repeat: no-repeat; background-size: cover; height: 100%; background-position: center center;">
				<div class="card card border-primary text-center mx-auto my-auto "style="width: 50%; height: auto;">
					<div class="col-md-12">
						<img src="${electeur.candidat.banniere}"   class="card-img-top img-fluid h-25 ">
					</div>
					<div class="card-body">
						<div class="card col-md-12">
							<div class="row no-gutters">
								<div class="col-md-4">
									<img src="${electeur.candidat.photo}"   class="card-img-top img-fluid">
								</div>
								
								<div class="card-body col-md-4 text-center">
										<h5 class="card-title">${electeur.candidat.nom} ${electeur.candidat.prenom}</h5>
										<p class="card-text">Bienvenue sur votre profil</p>	
										<c:url var="accueil" value="/accueil.html"/> 	
					    				<c:url var="modifier" value="/modifierProfil.html"/> 
					    				
					    				<div class="d-flex justify-content-around">
					    				<a href="${modifier}" class="card-link">Modifier profil</a>
										
										<a href="${accueil}" class="card-link">Accueil</a>
					    				</div>
								
								</div>
							
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</body>
</html>