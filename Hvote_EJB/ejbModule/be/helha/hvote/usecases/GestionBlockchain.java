package be.helha.hvote.usecases;

import java.util.ArrayList;

import javax.ejb.Remote;

import be.helha.hvote.domaine.Block;

@Remote
public interface GestionBlockchain {
	
	
	String isChainValid(ArrayList<Block> blockchain);

	Block rechercherBlock(String hash);

	Block sauver(Block bloc);
	
	

}