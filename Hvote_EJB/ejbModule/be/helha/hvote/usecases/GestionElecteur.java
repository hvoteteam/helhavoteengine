package be.helha.hvote.usecases;

import java.util.List;

import javax.ejb.Remote;

import be.helha.hvote.domaine.Electeur;

@Remote
public interface GestionElecteur {
	Electeur enregistrer(Electeur electeur);
	
	List<Electeur> listerElecteurs();
	
	Electeur rechercherElecteur(String pseudomail);
	
	List<Electeur> listerElecteurs(String mot);

	Electeur connecter(Electeur electeur);

	Electeur chargerTout(Electeur elect);

	void update(Electeur electeur);

}
