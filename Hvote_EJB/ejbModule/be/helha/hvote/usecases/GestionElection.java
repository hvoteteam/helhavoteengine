package be.helha.hvote.usecases;

import java.util.List;

import javax.ejb.Remote;

import be.helha.hvote.domaine.Block;
import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.domaine.Election;
import be.helha.hvote.domaine.Programme;
import be.helha.hvote.domaine.VoteEnt;

@Remote
public interface GestionElection {
	Election sauver(Election election);
	
	Election rechercherElection(String sujet);

	Election addProg(Election election, Programme prog);

	Election chargerTout(Election electi);
	
	List<Election> listerTout();

	Election chargerChain(Election elect);

	Election addBlock(Election election, Block block);

	Election chargerVotes(Election elec);

	Election addVote(Election election, VoteEnt vote);

	Election votedForProg(Election election, Programme prog);

	boolean hasAlreadyVote(Election election, Electeur electeur);

	Election nextTurn(Election election);

	Election sendToT2(Election election, Programme prog);

	Election votedForProgT2(Election election, Programme prog);

	boolean hasVotedT1(Election election, Electeur electeur);

	Election setCouleur(Election election, String couleur);

	Election setBanniere(Election election, String banniere);

	Election updateBanniere(Election election, String banniere);

	Election updateCouleur(Election election, String couleur);
	

}
