package be.helha.hvote.usecases;

import java.util.List;

import javax.ejb.Remote;

import be.helha.hvote.domaine.Electeur;

import be.helha.hvote.domaine.VoteEnt;

@Remote
public interface GestionVote {
	
	List<VoteEnt> listerVotes();
	
	VoteEnt sauver(VoteEnt vote);

	VoteEnt rechercherVote(int id);

	VoteEnt defineElecteur(VoteEnt vote, Electeur elec);
	
}