package be.helha.hvote.usecases;

import java.util.List;

import javax.ejb.Remote;

import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.domaine.Programme;

@Remote
public interface GestionProgramme {
	
	List<Programme> listerProgrammes();
	
	Programme sauver(Programme programme);

	Programme modifierProgTexte(String progTexte, String nouvTexte);

	Programme defineCandid(Programme prog, Electeur cand);

	Programme rechercherProg(int id);

	Programme addOneVote(Programme prog);

	Programme updateProgTexte(Programme prog, String nouvTexte);
}
