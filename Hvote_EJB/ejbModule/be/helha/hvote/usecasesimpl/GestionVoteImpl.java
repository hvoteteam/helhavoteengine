package be.helha.hvote.usecasesimpl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import be.helha.hvote.dao.ProgrammeDao;
import be.helha.hvote.dao.VoteDao;
import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.domaine.Election;
import be.helha.hvote.domaine.Programme;
import be.helha.hvote.domaine.VoteEnt;
import be.helha.hvote.usecases.GestionProgramme;
import be.helha.hvote.usecases.GestionVote;

@Stateless
public class GestionVoteImpl implements GestionVote{
	@EJB
	private VoteDao voteDao;

	@Override
	public List<VoteEnt> listerVotes() {
		return voteDao.lister();
	}

	@Override
	public VoteEnt sauver(VoteEnt vote) {
		
		return voteDao.enregistrer(vote);
	}

	@Override
	public VoteEnt rechercherVote(int id) {
		VoteEnt vote = this.voteDao.rechercher(id);
		return vote;
	}

	@Override
	public VoteEnt defineElecteur(VoteEnt vote, Electeur elec) {
		
		vote.setElecteur(elec);
		return vote;
	}
	
	

}
