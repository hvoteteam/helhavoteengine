package be.helha.hvote.usecasesimpl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import be.helha.hvote.dao.ProgrammeDao;
import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.domaine.Programme;
import be.helha.hvote.usecases.GestionProgramme;

@Stateless
public class GestionProgrammeImpl implements GestionProgramme{
	@EJB
	private ProgrammeDao progDao;

	@Override
	public List<Programme> listerProgrammes() {
		return progDao.lister();
	}

	@Override
	public Programme sauver(Programme programme) {
		
		return this.progDao.enregistrer(programme);
	}
	
	@Override
	public Programme modifierProgTexte(String progTexte, String nouvTexte) {
		Programme prog = this.progDao.rechercher(progTexte);
		if (prog == null)
			return null;
		prog.setTexteProgramme(nouvTexte);
		return prog;
	}
	
	@Override
	public Programme updateProgTexte(Programme prog, String nouvTexte) {
		
		prog.setTexteProgramme(nouvTexte);
		prog = this.progDao.mettreAJour(prog);
		return prog;
	}
	
	@Override
	public Programme defineCandid(Programme prog, Electeur cand) {
		
		prog.setCandidat(cand);
		return prog;
	}

	
	@Override
	public Programme rechercherProg(int id) {
		Programme prog = this.progDao.rechercher(id);
		return prog;
	}
	
	@Override
	public Programme addOneVote(Programme prog) {
		int cpt = prog.getCptVote();
		prog.setCptVote(cpt+1);
		return prog;
	}
	

}
