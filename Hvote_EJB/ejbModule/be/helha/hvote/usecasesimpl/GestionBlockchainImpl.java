package be.helha.hvote.usecasesimpl;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import be.helha.hvote.dao.BlockDao;
import be.helha.hvote.domaine.Block;
import be.helha.hvote.domaine.Election;
import be.helha.hvote.usecases.GestionBlockchain;

@Stateless
public class GestionBlockchainImpl implements GestionBlockchain{
	@EJB
	private BlockDao blocDao;
	
	@Override
	public Block rechercherBlock(String hash) {
		Block block = this.blocDao.rechercher(hash);
		return block;
	}
	
	@Override
	public Block sauver(Block bloc) {
		Block blocCheck = this.blocDao.rechercher(bloc.getHash());
		if(blocCheck != null) return bloc;
		
		return this.blocDao.enregistrer(bloc);
	}

	@Override
	public String isChainValid(ArrayList<Block> blockchain) {
		Block currentBlock; 
		Block previousBlock;
		String message="";

		//parcours de toute la blockchain pour tester les hashs et hashs precedent
		for(int i=1; i < blockchain.size(); i++) {
			currentBlock = blockchain.get(i);
			previousBlock = blockchain.get(i-1);
			//compare le hash calcule est different du hash enregistre
			if(!currentBlock.getHash().equals(currentBlock.calculateHash()) ){
				message = "Le hash calcul� est diff�rent du hash enregistr�";			
				return message;
			}
			//compare le hash precedent et le hash precedent enregistrer
			if(!previousBlock.getHash().equals(currentBlock.getPreviousHash()) ) {
				message = "Le hash pr�c�dent est diff�rent du hash pr�c�dent enregistr�";
				return message;
			}
		}
		return message = "Blockchain valide";
	}

}