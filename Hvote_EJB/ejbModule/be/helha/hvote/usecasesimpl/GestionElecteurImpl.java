package be.helha.hvote.usecasesimpl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;


import be.helha.hvote.dao.ElecteurDao;
import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.usecases.GestionElecteur;
import be.helha.hvote.util.Util;

@Stateless
public class GestionElecteurImpl implements GestionElecteur{
	@EJB
	private ElecteurDao electeurDao;

	@Override
	public Electeur enregistrer(Electeur electeur) {
		Electeur userCheck = this.electeurDao.rechercher(electeur.getPseudoMail());
		if(userCheck != null) return electeur;
		
		String passwordHashed = Util.hash(electeur.getPasswd());
		electeur.setPasswd(passwordHashed);
		return this.electeurDao.enregistrer(electeur);
	}
	
	@Override
	public Electeur connecter(Electeur electeur) {
		Electeur userDB = this.electeurDao.rechercher(electeur.getPseudoMail());
		if(userDB == null) return electeur;
		
		if(Util.checkHash(electeur.getPasswd(), userDB.getPasswd())) userDB.setConnecte(true);
		else return electeur;
		return userDB;
	}

	@Override
	public List<Electeur> listerElecteurs() {
		return this.electeurDao.lister();
	}

	@Override
	public Electeur rechercherElecteur(String pseudomail) {
		Electeur user = this.electeurDao.rechercher(pseudomail);
		return user;
	}

	@Override
	public List<Electeur> listerElecteurs(String mot) {
		return this.electeurDao.lister(mot);
	}
	
	@Override
	public Electeur chargerTout(Electeur elect) {
		return this.electeurDao.chargerProgrammes(elect);
	}
	
	@Override
	public void update(Electeur electeur) {
		Electeur Elecxist = this.electeurDao.rechercher(electeur.getPseudoMail());
		if(Elecxist!=null)this.electeurDao.mettreAJour(electeur); 
	}

}
