package be.helha.hvote.usecasesimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import be.helha.hvote.dao.ElectionDao;
import be.helha.hvote.dao.ProgrammeDao;
import be.helha.hvote.domaine.Block;
import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.domaine.Election;
import be.helha.hvote.domaine.Programme;
import be.helha.hvote.domaine.VoteEnt;
import be.helha.hvote.usecases.GestionElection;

@Stateless
public class GestionElectionImpl implements GestionElection{
	@EJB
	private ElectionDao electionDao;
	@EJB
	private ProgrammeDao progDao;


	@Override
	public Election sauver(Election election) {
		Election electionCheck = this.electionDao.rechercher(election.getSujet());
		if(electionCheck != null) return election;
		
		return this.electionDao.enregistrer(election);
	}

	@Override
	public Election rechercherElection(String sujet) {
		Election elec = this.electionDao.rechercher(sujet);
		return elec;
	}

	
	@Override
	public Election addProg(Election election, Programme prog) {
		List<Programme> list = null;
		//System.out.println("ele" +election);
		//System.out.println("pro"+prog);
		Election election2 = this.chargerTout(election);
		//System.out.println("ele2"+election2);
		//System.out.println("elepro2"+election2.getProgrammes());
		if(election2 == null) {
			list = new ArrayList<Programme>();
		}
		else {
			list = election2.getProgrammes();
		}
		
		list.add(prog);
		//System.out.println(list);
		election.setProgrammes(list);
		//System.out.println("ele2"+election);
		//System.out.println("elepro2"+election.getProgrammes());
		return election;
	}
	
	@Override
	public Election chargerTout(Election electi) {
		return this.electionDao.chargerProgrammes(electi);
	}
	
	@Override
	public Election chargerChain(Election elect) {
		return this.electionDao.chargerBlocks(elect);
	}
	
	@Override
	public Election chargerVotes(Election elec) {
		return this.electionDao.chargerVotes(elec);
	}

	@Override
	public List<Election> listerTout() {
		return this.electionDao.lister();
	}
	
	@Override
	public Election addBlock(Election election, Block block) {
		List<Block> list = null;
		
		Election election2 = this.chargerChain(election);
		
		if(election2 == null) {
			list = new ArrayList<Block>();
		}
		else {
			list = election2.getBlockchain();
		}
		
		list.add(block);
	
		election.setBlockchain(list);
		
		return election;
	}

	@Override
	public Election addVote(Election election, VoteEnt vote) {
		List<VoteEnt> list = null;
		
		Election election2 = this.chargerVotes(election);
		
		if(election2 == null) {
			list = new ArrayList<VoteEnt>();
		}
		else {
			list = election2.getVotes();
		}
		
		list.add(vote);
	
		election.setVotes(list);
		
		return election;
	}

	@Override
	public Election votedForProg(Election election, Programme prog) {
		Election elecCharg = this.electionDao.chargerProgrammes(election);
		List<Programme> listProg = elecCharg.getProgrammes();
		for(Programme p : listProg) {
			if(prog.getId()==p.getId()) {
				int cpt = p.getCptVote();
				p.setCptVote(cpt+1);
			}
		}
		
		return elecCharg;
		
	}
	
	@Override
	public Election votedForProgT2(Election election, Programme prog) {
		Election elecCharg = this.electionDao.chargerProgrammes(election);
		List<Programme> listProg = elecCharg.getProgrammes();
		for(Programme p : listProg) {
			if(prog.getId()==p.getId()) {
				int cpt = p.getCptVoteT2();
				p.setCptVoteT2(cpt+1);
			}
		}
		
		return elecCharg;
		
	}
	
	@Override
	public Election sendToT2(Election election, Programme prog) {
		Election elecCharg = this.electionDao.chargerProgrammes(election);
		List<Programme> listProg = elecCharg.getProgrammes();
		for(Programme p : listProg) {
			if(prog.getId()==p.getId()) {
				
				p.setGoToT2(1);
			}
		}
		
		return elecCharg;
		
	}
	
	@Override
	public boolean hasVotedT1(Election election, Electeur electeur) {
		int deja = 0;
		Election elecCharge = this.electionDao.chargerVotes(election);
		List<VoteEnt> listVote = elecCharge.getVotes();
		
		
		for(VoteEnt vot : listVote) {
			if(vot.getNbTour()==1) {		//le vote concerne tour 1
				if(vot.getElecteur().getPseudoMail().equals(electeur.getPseudoMail())) {
					deja = deja + 1;														//a vote au t1
				}
			}
		}
		
		if(deja==0) {
			
			return false;
		}
		else {
		
			return true;
		}
		
	}
	
	@Override
	public boolean hasAlreadyVote(Election election, Electeur electeur) {
		int deja = 0;
		
		int nbTours = election.getNbTours();
		int tour = election.getTourActuel();
		
		Election elecCharge = this.electionDao.chargerVotes(election);
		
		List<VoteEnt> listVote = elecCharge.getVotes();			//liste de vote de l election, qq soit le tour
		
		List<VoteEnt> listVoteT2 = new ArrayList<VoteEnt>();
		
		if(nbTours==1) {							//1 tour, verifie si deja vote ou pas
			for(VoteEnt vot : listVote) {
				
				if(vot.getElecteur().getPseudoMail().equals(electeur.getPseudoMail()) ) {
					
					deja = deja + 1;
			
				}
			}
		}
		
		if(nbTours==2) {							//2 tours
			
			if(tour==1) {								//on est tour 1 sur 2, verifie si deja vote
				for(VoteEnt vot : listVote) {
					
					if(vot.getElecteur().getPseudoMail().equals(electeur.getPseudoMail()) ) {
						
						deja = deja + 1;
				
					}
				}
			}
			if(tour==2) {
				for(VoteEnt vot : listVote) {
					if(vot.getNbTour()==2) {		//le vote concerne tour 2
						listVoteT2.add(vot);
					}
				}
				for(VoteEnt vott2 : listVoteT2) {
					if(vott2.getElecteur().getPseudoMail().equals(electeur.getPseudoMail()) ) {
						deja = deja + 1;
					}
				}
				
			}
			
		}
		
	
		
		if(deja==0) {
			
			return false;
		}
		else {
		
			return true;
		}
		
		
	}
	
	@Override
	public Election nextTurn(Election election) {
		int tour = election.getTourActuel();
		
		if(tour==1) {
			election.setTourActuel(2);
		}
		//else y a pas plus de 2 tours, on reste au tour 2
		return election;
		
	}
	
	@Override
	public Election setCouleur (Election election, String couleur) {
		election.setTheme(couleur);
		return election;
	}
	
	@Override
	public Election updateCouleur (Election election, String couleur) {
		election.setTheme(couleur);
		election = this.electionDao.mettreAJour(election);
		return election;
	}
	
	@Override
	public Election setBanniere (Election election, String banniere) {
		election.setBanniere(banniere);
		return election;
	}
	
	@Override
	public Election updateBanniere (Election election, String banniere) {
		election.setBanniere(banniere);
		election = this.electionDao.mettreAJour(election);
		return election;
	}

}
