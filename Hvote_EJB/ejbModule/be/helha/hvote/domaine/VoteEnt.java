package be.helha.hvote.domaine;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="VOTES", schema = "HVOTE")
public class VoteEnt implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column()
	private String date;
	@Column()
	private int nbTour;
	
	@ManyToOne
    private Electeur electeur ;
	
	
	public VoteEnt() {
		
	}


	public VoteEnt(int nbTour) {
		super();
		Date dat = new Date();
		this.date = dat.toString();
		this.nbTour = nbTour;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public int getNbTour() {
		return nbTour;
	}


	public void setNbTour(int nbTour) {
		this.nbTour = nbTour;
	}

	public Electeur getElecteur() {
		return electeur;
	}


	public void setElecteur(Electeur electeur) {
		this.electeur = electeur;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VoteEnt other = (VoteEnt) obj;
		if (id != other.id)
			return false;
		return true;
	}



}
