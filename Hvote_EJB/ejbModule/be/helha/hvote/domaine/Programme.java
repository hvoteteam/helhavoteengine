package be.helha.hvote.domaine;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import be.helha.hvote.exceptions.ArgumentInvalideException;
import be.helha.hvote.util.Util;


@SuppressWarnings("serial")
@Entity
@Table(name = "PROGRAMMES", schema = "HVOTE")
public class Programme implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column
	private String texteProgramme;
	
	@Column()
	private int cptVote;
	
	@Column()
	private int cptVoteT2;
	
	@ManyToOne
	@JoinColumn(name = "Electeur_pseudomail")
	private Electeur candidat;
	
	@Column()
	private int goToT2;
	
	
	public Programme() {
	}

	public Programme(String texteProgramme) throws ArgumentInvalideException{
		super();
		Util.checkString(texteProgramme);
		this.texteProgramme = texteProgramme;
		this.cptVote = 0;
		this.cptVoteT2 = 0;
		this.goToT2 = 0;
	}

	public Programme(String texteProgramme, int cptVote) throws ArgumentInvalideException{
		super();
		Util.checkString(texteProgramme);
		//Util.checkString(cptVote);
		this.texteProgramme = texteProgramme;
		this.cptVote = cptVote;
	}


	public String getTexteProgramme() {
		return texteProgramme;
	}


	public void setTexteProgramme(String texteProgramme) {
		this.texteProgramme = texteProgramme;
	}


	public int getCptVote() {
		return cptVote;
	}


	public void setCptVote(int cptVote) {
		this.cptVote = cptVote;
	}
	
	public int getCptVoteT2() {
		return cptVoteT2;
	}


	public void setCptVoteT2(int cptVoteT2) {
		this.cptVoteT2 = cptVoteT2;
	}
	
	public int getGoToT2() {
		return goToT2;
	}


	public void setGoToT2(int goToT2) {
		this.goToT2 = goToT2;
	}

	public Electeur getCandidat() {
		return candidat;
	}

	public void setCandidat(Electeur candidat) {
		this.candidat = candidat;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Programme [id =" + id + ", texteProgramme=" + texteProgramme + ", cptVote=" + cptVote + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((texteProgramme == null) ? 0 : texteProgramme.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Programme other = (Programme) obj;
		if (texteProgramme == null) {
			if (other.texteProgramme != null)
				return false;
		} else if (!texteProgramme.equals(other.texteProgramme))
			return false;
		return true;
	}


}