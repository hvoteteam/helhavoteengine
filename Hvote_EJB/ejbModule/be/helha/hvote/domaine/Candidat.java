package be.helha.hvote.domaine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import be.helha.hvote.exceptions.ArgumentInvalideException;

@SuppressWarnings("serial")
@Embeddable
public class Candidat implements Serializable{
	
	private String photo;
	
	private String banniere;
	
	private String nom; 
	
	private String prenom;
	
	@OneToMany(mappedBy="candidat", fetch=FetchType.LAZY)
	private List<Programme> programmes = new ArrayList<Programme>();
	
	
	public Candidat() {
	}


	public Candidat(String photo, String banniere) throws ArgumentInvalideException{
		super();
		this.photo = photo;
		this.banniere = banniere;
	}
	
	public Candidat(String nom, String prenom,String photo, String banniere) throws ArgumentInvalideException{
		super();
		this.nom = nom; 
		this.prenom = prenom;
		this.photo = photo;
		this.banniere = banniere;
	}
	
	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public String getBanniere() {
		return banniere;
	}


	public void setBanniere(String banniere) {
		this.banniere = banniere;
	}
	
	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public List<Programme> getProgrammes() {
		return programmes;
	}


	public void setProgrammes(List<Programme> programmes) {
		this.programmes = programmes;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((banniere == null) ? 0 : banniere.hashCode());
		result = prime * result + ((photo == null) ? 0 : photo.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Candidat other = (Candidat) obj;
		if (banniere == null) {
			if (other.banniere != null)
				return false;
		} else if (!banniere.equals(other.banniere))
			return false;
		if (photo == null) {
			if (other.photo != null)
				return false;
		} else if (!photo.equals(other.photo))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Candidat [photo=" + photo + ", banniere=" + banniere + "]";
	}




	
	

}