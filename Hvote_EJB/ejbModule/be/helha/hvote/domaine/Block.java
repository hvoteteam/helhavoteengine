package be.helha.hvote.domaine;

import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BLOCKS", schema = "HVOTE")
public class Block implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(nullable=false)
	private String hash;
	@Column()
	protected String previousHash;
	@Column(nullable=false)
	private byte[] data;
	@Column
	private SecretKey cle;
	
	public Block() {	
	}

	public Block(String newData, String previousHash, SecretKey cle) throws InvalidKeyException, NoSuchAlgorithmException, 
	NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		super();
		this.previousHash = previousHash;//stockage du hash precedent
		this.data = encrypter(newData, cle);//chiffrement des donnees a la construction du block
		this.hash = calculateHash(); //calcul du hash a la construction du block
		this.cle = cle;
		
	}
	

	public byte[] getData() {
		return this.data;
	}

	public String getHash() {
		return hash;
	}

	public String getPreviousHash() {
		return previousHash;
	}
	
	public SecretKey getCle() {
		return cle;
	}
	
	@Override
	public String toString() {
		return "Block [ previousHash = " + previousHash + ", data =  " + this.getData() + ", hash = " + hash + " ]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hash == null) ? 0 : hash.hashCode());
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Block other = (Block) obj;
		if (hash == null) {
			if (other.hash != null)
				return false;
		} else if (!hash.equals(other.hash))
			return false;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
	
	

	public String calculateHash() {
		String calculatedhash = this.applySha256(previousHash +	this.data);
		return calculatedhash;
	}
	
	//applique sha256 a une string et retourne le resultat
	public String applySha256(String input){		
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");	        
			//Applique sha256 a notre String 
			byte[] hash = digest.digest(input.getBytes("UTF-8"));	        
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if(hex.length() == 1) hexString.append('0');
				hexString.append(hex);
			}
			return hexString.toString();
		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	//methode pour chiffrer les donnees
	public static byte[] encrypter(final String message, SecretKey cle)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("DESede");
		cipher.init(Cipher.ENCRYPT_MODE, cle);
		byte[] donnees = message.getBytes();

		return cipher.doFinal(donnees);
	}
	
	//methode pour dechiffrer les donnees
	public  String decrypter(final byte[] donnees, SecretKey cle)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("DESede");
		cipher.init(Cipher.DECRYPT_MODE, cle);

		return new String(cipher.doFinal(donnees));
	}
	

}
