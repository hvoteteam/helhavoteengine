package be.helha.hvote.domaine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import be.helha.hvote.exceptions.ArgumentInvalideException;
import be.helha.hvote.util.Util;

@SuppressWarnings("serial")
@Entity
@Table(name = "ELECTIONS", schema = "HVOTE")
public class Election implements Serializable {
	
	
	@Id
	private String sujet;
	
	@ManyToOne
	private Electeur admin;
	
	@OneToMany(fetch=FetchType.LAZY)
	@JoinColumn(name="ELECTION_ID")
	private List<Programme> programmes = new ArrayList<Programme>();

	@OneToMany(fetch=FetchType.LAZY)
	@JoinColumn(name="ELECTION_ID")
	private List<VoteEnt> votes = new ArrayList<VoteEnt>();
	
	@OneToMany(fetch=FetchType.LAZY)
	@JoinColumn(name="ELECTION_ID")
	private List<Block> blockchain = new ArrayList<Block>();
	
	@Column()
	private Date startDate1;
	@Column()
	private Date startDate2;
	
	@Column()
	private Date endDate1;
	@Column()
	private Date endDate2;
	
	@Column()
	private int nbTours;
	
	@Column()
	private int tourActuel;
	
	@Column()
	private int cantVoteT2IfNoT1;
	
	@Column
	private String banniere;
	
	@Column
	private String theme;
	
	
	public Election() {
	}

	public Election(String sujet) throws ArgumentInvalideException{
		super();
		Util.checkString(sujet);
		this.sujet = sujet;
	}
	
	public Election(String sujet, Electeur admin) throws ArgumentInvalideException{
		super();
		Util.checkString(sujet);
		Util.checkObject(admin);
		this.sujet = sujet;
		this.admin = admin;
	}
	
	public Election(String sujet, Electeur admin, int nbTours) throws ArgumentInvalideException{
		super();
		Util.checkString(sujet);
		Util.checkObject(admin);
		this.sujet = sujet;
		this.admin = admin;
		this.nbTours = nbTours;
		this.tourActuel = 1;
	}
	
	public Election(String sujet, Electeur admin, int nbTours, int cantVoteT2IfNoT1) throws ArgumentInvalideException{
		super();
		Util.checkString(sujet);
		Util.checkObject(admin);
		this.sujet = sujet;
		this.admin = admin;
		this.nbTours = nbTours;
		this.tourActuel = 1;
		this.cantVoteT2IfNoT1 = cantVoteT2IfNoT1;
	}

	public String getSujet() {
		return sujet;
	}

	public void setSujet(String sujet) {
		this.sujet = sujet;
	}

	public Electeur getAdmin() {
		return admin;
	}

	public void setAdmin(Electeur admin) {
		this.admin = admin;
	}
	
	public List<Programme> getProgrammes() {
		return programmes;
	}

	public void setProgrammes(List<Programme> programmes) {
		this.programmes = programmes;
	}
	
	public List<VoteEnt> getVotes() {
		return votes;
	}

	public void setVotes(List<VoteEnt> votes) {
		this.votes = votes;
	}
	
	public List<Block> getBlockchain() {
		return blockchain;
	}

	public void setBlockchain(List<Block> blockchain) {
		this.blockchain = blockchain;
	}
	
	public Date getStartDate1() {
		return startDate1;
	}

	public void setStartDate1(Date startDate1) {
		this.startDate1 = startDate1;
	}

	public Date getStartDate2() {
		return startDate2;
	}

	public void setStartDate2(Date startDate2) {
		this.startDate2 = startDate2;
	}

	public Date getEndDate1() {
		return endDate1;
	}

	public void setEndDate1(Date endDate1) {
		this.endDate1 = endDate1;
	}

	public Date getEndDate2() {
		return endDate2;
	}

	public void setEndDate2(Date endDate2) {
		this.endDate2 = endDate2;
	}

	public int getNbTours() {
		return nbTours;
	}

	public void setNbTours(int nbTours) {
		this.nbTours = nbTours;
	}
	
	public int getTourActuel() {
		return tourActuel;
	}

	public void setTourActuel(int tourActuel) {
		this.tourActuel = tourActuel;
	}
	
	public int getCantVoteT2IfNoT1() {
		return cantVoteT2IfNoT1;
	}

	public void setCantVoteT2IfNoT1(int cantVoteT2IfNoT1) {
		this.cantVoteT2IfNoT1 = cantVoteT2IfNoT1;
	}
	
	public String getBanniere() {
		return banniere;
	}

	public void setBanniere(String banniere) {
		this.banniere = banniere;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	@Override
	public String toString() {
		return "Election [sujet=" + sujet + ", admin=" + admin + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sujet == null) ? 0 : sujet.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Election other = (Election) obj;
		if (sujet == null) {
			if (other.sujet != null)
				return false;
		} else if (!sujet.equals(other.sujet))
			return false;
		return true;
	}

	

	
	
	

	

}
