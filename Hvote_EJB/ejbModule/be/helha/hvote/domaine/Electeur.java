package be.helha.hvote.domaine;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import be.helha.hvote.exceptions.ArgumentInvalideException;
import be.helha.hvote.util.Util;

@SuppressWarnings("serial")
@Entity
@Table(name = "ELECTEURS", schema = "HVOTE")
public class Electeur implements Serializable {
	
	@Transient
	private boolean connecte = false;
	
	@Id
	private String pseudomail;
	
	@Column(nullable=false)
	private String passwd;
	
	@Embedded
	private Candidat candidat;
	
	
	public Electeur() {
	}


	public Electeur(String pseudomail, String passwd) throws ArgumentInvalideException{
		super();
		Util.checkString(pseudomail);
		Util.checkString(passwd);
		this.pseudomail = pseudomail;
		this.passwd = passwd;
	}


	public Electeur(boolean connecte, String pseudomail, String passwd) throws ArgumentInvalideException{
		super();
		Util.checkString(pseudomail);
		Util.checkString(passwd);
		this.connecte = connecte;
		this.pseudomail = pseudomail;
		this.passwd = passwd;
	}

	public boolean isConnecte() {
		return connecte;
	}


	public void setConnecte(boolean connecte) {
		this.connecte = connecte;
	}


	public String getPseudoMail() {
		return pseudomail;
	}


	public void setPseudoMail(String pseudomail) {
		this.pseudomail = pseudomail;
	}


	public String getPasswd() {
		return passwd;
	}


	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	
	public Candidat getCandidat() {
		return candidat;
	}


	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((passwd == null) ? 0 : passwd.hashCode());
		result = prime * result + ((pseudomail == null) ? 0 : pseudomail.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Electeur other = (Electeur) obj;
		if (passwd == null) {
			if (other.passwd != null)
				return false;
		} else if (!passwd.equals(other.passwd))
			return false;
		if (pseudomail == null) {
			if (other.pseudomail != null)
				return false;
		} else if (!pseudomail.equals(other.pseudomail))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Electeur [connecte=" + connecte + ", pseudoMail=" + pseudomail + ", passwd=" + passwd + "]";
	}


	
	

}
