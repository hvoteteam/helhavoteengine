package be.helha.hvote.dao;

import java.util.List;

import javax.ejb.Local;

import be.helha.hvote.domaine.Electeur;

@Local
public interface ElecteurDao extends Dao<String, Electeur>{

	Electeur rechercher(String pseudo);
	
	Electeur rechercherPswd(String pswd);
	
	List<Electeur> lister(String mot);

	Electeur chargerProgrammes(Electeur elect);
	
	
}
