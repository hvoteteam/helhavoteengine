package be.helha.hvote.dao;

import javax.ejb.Local;

import be.helha.hvote.domaine.Block;


@Local
public interface BlockDao extends Dao<String, Block>{
	
	Block rechercher(String hash);
	
	
}