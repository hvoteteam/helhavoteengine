package be.helha.hvote.dao;

import java.io.Serializable;
import java.util.List;

public interface Dao<K,E> extends Serializable {
	E rechercher(K id);
	E enregistrer(E entit้);
	E mettreAJour(E entit้);
	E recharger(K id);
	void supprimer(K id);
	List<E> lister();
}
