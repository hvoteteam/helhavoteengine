package be.helha.hvote.dao;

import java.util.List;

import javax.ejb.Local;

import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.domaine.Election;

@Local
public interface ElectionDao extends Dao<String, Election>{
	
	Election rechercher(String sujet);

	Election chargerProgrammes(Election electi);
	
	List<Election> lister();

	Election chargerBlocks(Election elect);

	Election chargerVotes(Election elec);
	
}
