package be.helha.hvote.dao;

import java.util.List;

import javax.ejb.Local;

import be.helha.hvote.domaine.Programme;


@Local
public interface ProgrammeDao extends Dao<String, Programme>{

	Programme rechercher(int id);
}
