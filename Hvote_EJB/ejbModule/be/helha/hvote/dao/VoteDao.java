package be.helha.hvote.dao;

import javax.ejb.Local;

import be.helha.hvote.domaine.Block;
import be.helha.hvote.domaine.VoteEnt;


@Local
public interface VoteDao extends Dao<String, VoteEnt>{
	
	VoteEnt rechercher(int id);
	
	
}