package be.helha.hvote.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.mindrot.jbcrypt.BCrypt;

import be.helha.hvote.exceptions.ArgumentInvalideException;

public final class Util {
	public static void checkObject(Object o) throws ArgumentInvalideException {
		if (o == null)
			throw new ArgumentInvalideException();
	}

	public static void checkString(String s) throws ArgumentInvalideException {
		checkObject(s);
		if (s.trim().equals(""))
			throw new ArgumentInvalideException();
	}

	public static void checkNegativeOrZero(double d)
			throws ArgumentInvalideException {
		if (d > 0.0)
			throw new ArgumentInvalideException();
	}

	public static void checkPositiveOrZero(double d)
			throws ArgumentInvalideException {
		if (d < 0)
			throw new ArgumentInvalideException();
	}

	public static void checkPositive(double d) throws ArgumentInvalideException {
		if (d <= 0.00001)
			throw new ArgumentInvalideException();
	}

	public static void checkPositive(int i) throws ArgumentInvalideException {
		if (i <= 0)
			throw new ArgumentInvalideException();
	}
	
	public static String hash(String string) {
		return BCrypt.hashpw(string, BCrypt.gensalt());
	}
	
	public static boolean checkHash(String string1, String string2) {
		return BCrypt.checkpw(string1, string2);
	}
	
	public static boolean sauverImage(int[] image, String path) {
		boolean sauve = false;
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		try {
			fos = new FileOutputStream(path + "uneImage.PNG");
			bos = new BufferedOutputStream(fos);
			for (int b : image) {
				bos.write(b);
			}
			sauve = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (bos!=null) bos.close();
			} catch (IOException e) {
				e.printStackTrace();
				sauve = false;
			}
		}
		return sauve;
	}

	public static boolean sauverImage(InputStream is, String path) {
		boolean sauve = false;

		BufferedInputStream bis = new BufferedInputStream(is);
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		try {
			fos = new FileOutputStream(path);
			bos = new BufferedOutputStream(fos);
			int r;
			while ((r = bis.read()) != -1) {
				bos.write(r);
			}
			sauve = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (bos!=null) bos.close();
				if (bis!=null)bis.close();
			} catch (IOException e) {
				e.printStackTrace();
				sauve = false;
			}
		}
		return sauve;
	}



	public static String lectureTexte(String path) {
		String texte = "";
		FileInputStream fis = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		String line;

		try {
			fis = new FileInputStream(path);
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);
			while ((line = br.readLine()) != null) {
				texte+=line+"\n";
			}
		} catch (Exception e) {
			e.printStackTrace();
			texte = "Erreur lors de la lecture du fichier";
		} finally {
			try {
				if (br!=null) br.close();
				if (fis!=null) fis.close();
			} catch (Exception e) {
				e.printStackTrace();
				texte = "Erreur lors de la lecture du fichier";
			}
		}
		return texte;
	}
}
