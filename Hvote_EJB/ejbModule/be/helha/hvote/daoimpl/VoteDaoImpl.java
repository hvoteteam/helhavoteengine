package be.helha.hvote.daoimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import be.helha.hvote.dao.BlockDao;
import be.helha.hvote.dao.VoteDao;
import be.helha.hvote.domaine.Block;
import be.helha.hvote.domaine.VoteEnt;


@SuppressWarnings("serial")
@Stateless
public class VoteDaoImpl extends DaoImpl<String,VoteEnt> implements VoteDao{


	@Override
	public VoteEnt rechercher(int id) {
		String queryString="select e from VoteEnt e where e.id = ?1";
		return recherche(queryString, id);
	}

}