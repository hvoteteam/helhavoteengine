package be.helha.hvote.daoimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import be.helha.hvote.dao.BlockDao;
import be.helha.hvote.domaine.Block;


@SuppressWarnings("serial")
@Stateless
public class BlockDaoImpl extends DaoImpl<String,Block> implements BlockDao{


	@Override
	public Block rechercher(String hash) {
		String queryString="select e from Block e where e.hash = ?1";
		return recherche(queryString, hash);
	}

}