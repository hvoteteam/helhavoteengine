package be.helha.hvote.daoimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import be.helha.hvote.dao.ElectionDao;
import be.helha.hvote.domaine.Electeur;
import be.helha.hvote.domaine.Election;

@SuppressWarnings("serial")
@Stateless
public class ElectionDaoImpl extends DaoImpl<String,Election> implements ElectionDao{

	@Override
	public Election rechercher(String sujet) {
		String queryString="select e from Election e where e.sujet = ?1";
		return recherche(queryString, sujet);
	}	
	
	@Override
	public Election chargerProgrammes(Election electi) {
		Election electiCharge = rechercher(electi.getSujet());
		if ( electiCharge==null) return null;
		electiCharge.getProgrammes().size();
		return electiCharge;
	}
	
	@Override
	public List<Election> lister() {
		return super.lister();
}

	@Override
	public Election chargerBlocks(Election elect) {
		
		Election electiCharge = rechercher(elect.getSujet());
		
		if (electiCharge==null) return null;
		electiCharge.getBlockchain().size();
		return electiCharge;
	}
	
	@Override
	public Election chargerVotes(Election elec) {
		
		Election electiCharge = rechercher(elec.getSujet());
		
		if (electiCharge==null) return null;
		electiCharge.getVotes().size();
		return electiCharge;
	}
	

}
