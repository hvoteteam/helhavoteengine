package be.helha.hvote.daoimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import be.helha.hvote.dao.ProgrammeDao;
import be.helha.hvote.domaine.Programme;

@SuppressWarnings("serial")
@Stateless
public class ProgrammeDaoImpl extends DaoImpl<String,Programme> implements ProgrammeDao{

	@Override
	public Programme rechercher(int id) {
		String queryString="select e from Programme e where e.id = ?1";
		return recherche(queryString, id);
	}	
}
