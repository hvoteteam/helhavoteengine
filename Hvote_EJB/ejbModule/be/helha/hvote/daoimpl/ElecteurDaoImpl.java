package be.helha.hvote.daoimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import be.helha.hvote.dao.ElecteurDao;
import be.helha.hvote.domaine.Electeur;

@SuppressWarnings("serial")
@Stateless
public class ElecteurDaoImpl extends DaoImpl<String,Electeur> implements ElecteurDao{

	@Override
	public Electeur rechercher(String pseudomail) {
		String queryString="select e from Electeur e where e.pseudomail = ?1";
		return recherche(queryString, pseudomail);
	}

	@Override
	public Electeur rechercherPswd(String pswd) {
		String queryString="select e from Electeur e where e.passwd = ?1";
		return recherche(queryString, pswd);
	}

	@Override
	public List<Electeur> lister(String mot) {
		List<Electeur> listeDB=lister();
		List<Electeur> listeMot = new ArrayList<Electeur>(2);
		for (Electeur b : listeDB) {
			String stringUser = b.toString(); 
			if ( stringUser.contains(mot)) listeMot.add(b);
		}
		return listeMot;
	}
	
	@Override
	public Electeur chargerProgrammes(Electeur elect) {
		Electeur electCharge = rechercher(elect.getPseudoMail());
		if ( electCharge==null) return null;
		electCharge.getCandidat().getProgrammes().size();
		return electCharge;
	}

}
