package be.helha.hvote.filtres;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class FiltreEbar
 */
@WebFilter(dispatcherTypes = { DispatcherType.REQUEST }, urlPatterns = { "/index.html", "*.html" })
public class FiltreHtml implements Filter {

	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		// Cast des objets request et response
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;

		// R�cup�ration du path de la requ�te
		String nom = request.getServletPath();
		nom = nom.substring(1); // on enl�ve le "/" du d�but
		if (!nom.isEmpty()) {
			RequestDispatcher rd = request.getServletContext()
					.getNamedDispatcher(nom);
			if (rd != null) {
				rd.forward(request, response);
				return;
			}
		}
		// redirection vers la page d'accueil
		response.sendRedirect(request.getContextPath() + "/accueil.html");
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

	public void destroy() {
	}
}
